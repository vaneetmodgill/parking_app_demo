//
//  YPDatabaseRequest.swift
//  WedMe
//
//  Created by Vaneet on 27/03/18.
//  Copyright © 2018 YapApp India Pvt. Ltd. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct YPDatabaseRequest {
    var reference: DatabaseReference
    var task: YPDatabaseTask
    var onDisconnect: Bool
}

extension YPDatabaseRequest {
    init(_ target: YPDatabaseTarget) {
        
        
        self.reference = target.path == "" ? target.baseReference : target.baseReference.child(target.path)
        self.task = target.task
        self.onDisconnect = target.onDisconnect
    }
}

struct YPDatabaseQueryRequest {
    var query: DatabaseQuery
    var task: YPDatabaseTask
}

extension YPDatabaseQueryRequest {
    init(_ target: YPDatabaseTarget) {
        let reference = target.path == "" ? target.baseReference : target.baseReference.child(target.path)
        self.query = target.queries?.reduce(reference) { $1.prepare($0) } ?? reference
        self.task = target.task
    }
}

