//
//  YPDatabaseResponse.swift
//  WedMe
//
//  Created by Vaneet on 27/03/18.
//  Copyright © 2018 YapApp India Pvt. Ltd. All rights reserved.
//

import Foundation
import FirebaseDatabase
public protocol JSONDecodeable {
    init?(_ json: [String: Any])
}

public struct YPDatabaseResponse {
    public let snapshot: DataSnapshot?
    public let reference: DatabaseReference
    public let isCommitted: Bool
}

public extension YPDatabaseResponse {
    init(reference: DatabaseReference, snapshot: DataSnapshot? = nil, isCommitted: Bool = false) {
        self.reference = reference
        self.snapshot = snapshot
        self.isCommitted = isCommitted
    }
}

public extension YPDatabaseResponse {
    var json: [String: AnyObject]? {
        return snapshot?.value as? [String: AnyObject]
    }
  
}
