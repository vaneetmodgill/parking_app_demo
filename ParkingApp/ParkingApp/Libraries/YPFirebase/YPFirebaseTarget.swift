//
//  FirebaseTarget.swift
//  WedMe
//
//  Created by Vaneet on 27/03/18.
//  Copyright © 2018 YapApp India Pvt. Ltd. All rights reserved.
//

import Foundation
import FirebaseDatabase

public protocol YPFirebaseTarget {}

public protocol YPDatabaseTarget: YPFirebaseTarget {
    var baseReference: DatabaseReference { get }
    var path: String { get }
    var task: YPDatabaseTask { get }
    var queries: [YPDatabaseQuery]? { get }
    var onDisconnect: Bool { get }  // Defaults to false
}

public extension YPDatabaseTarget {
    var queries: [YPDatabaseQuery]? {
        return nil
    }
    var onDisconnect: Bool {
        return false
    }
}
