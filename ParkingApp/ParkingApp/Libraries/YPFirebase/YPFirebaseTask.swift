//
//  YPDatabaseTask.swift
//  WedMe
//
//  Created by Vaneet on 27/03/18.
//  Copyright © 2018 YapApp India Pvt. Ltd. All rights reserved.
//

import Foundation
import FirebaseDatabase

public enum YPDatabaseTask {
    case observe(DataEventType)
    case observeOnce(DataEventType)
    case setValue(Any?)
    case updateChildValues([AnyHashable: Any])
    case removeValue
}

public enum YPDatabaseQuery {
    case limitedFirst(UInt)
    case limitedLast(UInt)
    case orderedByChild(key: String) 
    case orderedByKey
    case orderedByValue
    case orderedByPriority
    case startingAtValue(Any?)
    case startingAt(value: Any?, childKey: String?)
    case endingAtValue(value: Any?)
    case endingAt(value: Any?, childKey: String?)
    case equalToValue(Any?)
    case equalTo(value: Any, childKey: String?)

}

public extension YPDatabaseQuery {

  func prepare(_ query: DatabaseQuery) -> DatabaseQuery {
    switch self {
      case .limitedFirst(let n):
        return query.queryLimited(toFirst: n)
      case .limitedLast(let n):
        return query.queryLimited(toLast: n)
      case .orderedByChild(let key):
        return query.queryOrdered(byChild: key)
      case .orderedByKey:
        return query.queryOrderedByKey()
      case .orderedByValue:
        return query.queryOrderedByValue()
      case .orderedByPriority:
        return query.queryOrderedByPriority()
      case .startingAtValue(let value):
        return query.queryStarting(atValue: value)
      case .startingAt(let value, let childKey):
        return query.queryStarting(atValue: value, childKey: childKey)
      case .endingAtValue(let value):
        return query.queryEnding(atValue: value)
      case .endingAt(let value, let childKey):
        return query.queryEnding(atValue: value, childKey: childKey)
      case .equalToValue(let value):
        return query.queryEqual(toValue: value)
      case .equalTo(let value, let childKey):
        return query.queryEqual(toValue: value, childKey: childKey)
    }
  }

}

