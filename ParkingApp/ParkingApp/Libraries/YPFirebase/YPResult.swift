//
//  YPResult.swift
//  WedMe
//
//  Created by Vaneet on 27/03/18.
//  Copyright © 2018 YapApp India Pvt. Ltd. All rights reserved.
//
import Foundation

public enum YPResult<Value> {
    case success(Value)
    case failure(Error)
}

public extension YPResult {
    init(value: Value) {
        self = .success(value)
    }
    init(error: Error) {
        self = .failure(error)
    }
    
}
