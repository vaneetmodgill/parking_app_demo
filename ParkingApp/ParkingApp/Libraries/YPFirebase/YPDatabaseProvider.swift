//
//  YPDatabaseProvider.swift
//  WedMe
//
//  Created by Vaneet on 27/03/18.
//  Copyright © 2018 YapApp India Pvt. Ltd. All rights reserved.
//

import Foundation
import FirebaseDatabase

public typealias DatabaseCompletion = (YPResult<YPDatabaseResponse>) -> Void

public class YPDatabaseProvider<Target: YPDatabaseTarget> {
    @discardableResult
    public func request(_ target: Target, completion: @escaping DatabaseCompletion = { _ in }) -> UInt? {
        var handle: UInt?
        switch target.task {
        case .observe, .observeOnce:
            let request = YPDatabaseQueryRequest(target)
            handle = processObserve(request, completion)
        case .setValue, .updateChildValues, .removeValue:
            let request = YPDatabaseRequest(target)
            processWrite(request, completion)
        }
        return handle
    }
    
    private func processObserve(_ request: YPDatabaseQueryRequest, _ completion: @escaping DatabaseCompletion) -> UInt? {
        let successMapping = { (snapshot: DataSnapshot) in
            let result = self.convertResponseToResult(snapshot: snapshot, reference: request.query.ref, error: nil)
            completion(result)
        }
        let failureMapping = { (error: Error?) in
            let result = self.convertResponseToResult(snapshot: nil, reference: nil, error: error)
            completion(result)
        }
        var handle: UInt?
        switch request.task {
        case .observe(let event):
            handle = request.query.observe(event, with: successMapping, withCancel: failureMapping)
        case .observeOnce(let event):
            request.query.observeSingleEvent(of: event, with: successMapping, withCancel: failureMapping)
        default:
            completion(.failure(YPError.requestMapping))
        }
        return handle
    }
    
    private func processWrite(_ request: YPDatabaseRequest, _ completion: @escaping DatabaseCompletion) {
        let completionBlock = { (error: Error?, reference: DatabaseReference) in
            let result = self.convertResponseToResult(snapshot: nil, reference: reference, error: error)
            completion(result)
        }
        switch request.task {
        case .setValue(let value):
            if request.onDisconnect {
                request.reference.onDisconnectSetValue(value, withCompletionBlock: completionBlock)
            } else {
                request.reference.setValue(value, withCompletionBlock: completionBlock)
            }
        case .updateChildValues(let values):
            if request.onDisconnect {
                request.reference.onDisconnectUpdateChildValues(values, withCompletionBlock: completionBlock)
            } else {
                request.reference.updateChildValues(values, withCompletionBlock: completionBlock)
            }
        case .removeValue:
            if request.onDisconnect {
                request.reference.onDisconnectRemoveValue(completionBlock: completionBlock)
            } else {
                request.reference.removeValue(completionBlock: completionBlock)
            }
        default:
            completion(.failure(YPError.requestMapping))
        }
    }
}

private extension YPDatabaseProvider {

    func convertResponseToResult(snapshot: DataSnapshot?, reference: DatabaseReference?, error: Error?, committed: Bool? = nil) -> YPResult<YPDatabaseResponse> {
        switch (snapshot, reference, error, committed) {
        case let (snapshot, .some(reference), .none, .some(committed)):
            let response = YPDatabaseResponse(reference: reference, snapshot: snapshot, isCommitted: committed)
            return .success(response)
        case let (.some(snapshot), .some(reference), .none, _):
            let response = YPDatabaseResponse(reference: reference, snapshot: snapshot, isCommitted: true)
            return .success(response)
        case let (.none, .some(reference), .none, _):
            let response = YPDatabaseResponse(reference: reference, isCommitted: true)
            return .success(response)
        case let (.none, _, .some(error), _):
            return .failure(YPError.underlying(error))
        default:
            return .failure(YPError.resultConversion)
        }
    }
    
}
