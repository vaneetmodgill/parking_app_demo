//
//  YPError.swift
//  WedMe
//
//  Created by Vaneet on 27/03/18.
//  Copyright © 2018 YapApp India Pvt. Ltd. All rights reserved.
//

import Foundation

public enum YPError: Error {
    case resultConversion
    case requestMapping
    case nullSnapshot
    case jsonMapping
    case objectDecoding
    case underlying(Error)
}

extension YPError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .resultConversion:
            return "There was an error converting the Response to a Result."
        case .requestMapping:
            return "There was an error mapping the Request to Firebase."
        case .nullSnapshot:
            return "The FIRDataSnapshot is empty."
        case .jsonMapping:
            return "The FIRDataSnapshot to JSON conversion failed."
        case .objectDecoding:
            return "The object decoding for the Response failed."
        case .underlying(let error):
            return error.localizedDescription
        }
    }
}
