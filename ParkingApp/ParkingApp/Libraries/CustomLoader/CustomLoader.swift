import Foundation
import UIKit

class CustomLoader {
    
    static let sharedInstance = CustomLoader()
    
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()

    var titleLabel: UILabel = UILabel()
    /*
        Show customized activity indicator,
        actually add activity indicator to passing view
    
        @param uiView - add activity indicator to this view
    */
    func showActivityIndicator(uiView: UIView, title:String, isNavigationBarHidden: Bool = true) {
        container.frame = uiView.bounds
       
        container.center = uiView.center
        if !isNavigationBarHidden{
            container.center = CGPoint(x:uiView.center.x, y:uiView.center.y - 64)
        }
        container.backgroundColor = UIColorFromHex(rgbValue: 0xffffff, alpha: 0.3)
        let height = heightForView(text: title, width: 100) + 40
        
        titleLabel.frame = CGRect(x:10.0, y:50.0, width:100.0, height:height)
        titleLabel.numberOfLines = 0
        titleLabel.text = title
        titleLabel.font = UIFont(name: "Helvetica", size: 20.0)
        titleLabel.textColor = .white
        titleLabel.textAlignment = .center
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.lineBreakMode = .byWordWrapping
        if title.count == 0{
            activityIndicator.frame = CGRect(x:0.0, y:30.0, width:20.0, height:40.0)
            activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: (loadingView.frame.size.height / 2))


        }else{
            loadingView.frame = CGRect(x: 0, y: 0, width: 120, height: height + 40)
            activityIndicator.frame = CGRect(x:0.0, y:0.0, width:20.0, height:40.0)
            activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: (loadingView.frame.size.height / 2)-15)


        }
        loadingView.center = uiView.center
        if !isNavigationBarHidden{
            loadingView.center = CGPoint(x:uiView.center.x, y:uiView.center.y - 64)
        }
        loadingView.backgroundColor = UIColorFromHex(rgbValue: 0x444444, alpha: 0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
    
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
 
        
        loadingView.addSubview(activityIndicator)
        
        if title.count != 0{
            loadingView.addSubview(titleLabel)
        }
        container.addSubview(loadingView)
        uiView.addSubview(container)
        activityIndicator.startAnimating()
    }

    func heightForView(text:String, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x:10.0, y:50.0, width:100.0, height:CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = UIFont(name: "Helvetica", size: 20.0)
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
        

    

    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }

    /*
        Define UIColor from hex value
        
        @param rgbValue - hex color value
        @param alpha - transparency level
    */
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
}

