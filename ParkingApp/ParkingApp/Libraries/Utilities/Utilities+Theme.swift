//
//  Utilities+Keyboard.swift
//  Nehao
//
//  Created by Ajay Kumar on 20/06/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import UIKit
extension(Utilities){
    static func getThemeColor() -> UIColor {
        return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    static func getThemeRedColor() -> UIColor {
        return #colorLiteral(red: 0.999522984, green: 0.1301312745, blue: 0.2501136065, alpha: 1)
    }
    static func getRedThemeColor() -> UIColor {
        return UtiltityMethods.colorWithHexString(hex: "#EF233C")
    }

    static func getPendingColor() -> UIColor {
        return UtiltityMethods.colorWithHexString(hex: "#FB9C00")
    }
    static func getCompletedColor() -> UIColor {
        return UtiltityMethods.colorWithHexString(hex: "#03C03F")
    }
    static func getRejectedColor() -> UIColor {
        return UtiltityMethods.colorWithHexString(hex: "#EA0023")
    }
    static func getInProcessColor() -> UIColor {
        return UtiltityMethods.colorWithHexString(hex: "#FDAE04")
    }
    static func getInProcessDarkColor() -> UIColor {
        return UtiltityMethods.colorWithHexString(hex: "#986904")
    }
    static func getLightGrayColor() -> UIColor {
        return UtiltityMethods.colorWithHexString(hex: "#dcdcdc") //E1E1E1
    }
    static func getGrayColor() -> UIColor {
        return UtiltityMethods.colorWithHexString(hex: "#B4B4B4") //E1E1E1
    }
    static func getButtonColor() -> UIColor {
        return UtiltityMethods.colorWithHexString(hex: "#263238")
    }
    static func getPaymentThemeColor() -> UIColor {
        return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    static func getBlueThemeColor() -> UIColor {
        return UtiltityMethods.colorWithHexString(hex: "#121343")
    }
    static func getLightBlueThemeColor() -> UIColor {
        return UtiltityMethods.colorWithHexString(hex: "#9797AC")
    }
    static func getSuggestionsThemeColor() -> UIColor {
        return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    static func getDarkThemeColor() -> UIColor {
        return UtiltityMethods.colorWithHexString(hex: "#F12844")
    }
    
    static func getRedBorderLayerColor() -> CGColor {
        return UtiltityMethods.colorWithHexString(hex: "#EF233C").cgColor
    }
    
    static func getGrayBorderLayerColor() -> CGColor {
        return UtiltityMethods.colorWithHexString(hex: "#dcdcdc").cgColor
    }
    
    static func getDarkGrayColor() -> UIColor {
        return UtiltityMethods.colorWithHexString(hex: "#969696")
    }
    
    static func getWedMeThemeColor() -> UIColor {
        return UtiltityMethods.colorWithHexString(hex: "#FD786B")
    }
    
    static func getWedMeLightThemeColor() -> UIColor {
        return UtiltityMethods.colorWithHexString(hex: "#FFDDDA")
    }
}
