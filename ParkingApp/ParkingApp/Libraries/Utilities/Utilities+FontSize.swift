//
//  Utilities+FontSize.swift
//  Nehao
//
//  Created by Ajay Kumar on 02/06/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import UIKit
extension(Utilities){
    enum FontViewType{
        case userCell
        case textView
        case label
        case messageCell
    }
    static func getFontSize(viewType:FontViewType,width:CGFloat)->CGFloat{
        if viewType == FontViewType.userCell{
            return self.getUserCellFontSize(width: width)
        }
        return 14
    }
    static func getFontSizeForChat(viewType:FontViewType,chatMessage:String, defaultFontSize:CGFloat)->CGFloat{
        if viewType == FontViewType.messageCell{
            return getMessageCellFontSize(chatMessage: chatMessage, defaultFontSize: defaultFontSize)
        }
        return defaultFontSize
    }
    
    private static func getMessageCellFontSize(chatMessage:String, defaultFontSize:CGFloat)->CGFloat{
        if !chatMessage.containsOnlyEmoji{
            return defaultFontSize
        }
        switch chatMessage.glyphCount {
        case 1:
            return 40
        case 2:
            return 30
        case 3:
            return 20
        default:
            return defaultFontSize
        }
    }
    
    private static func getUserCellFontSize(width:CGFloat)->CGFloat{
        if width >= (UIScreen.main.bounds.width)/2 {
            return 14
        }else if width >= (UIScreen.main.bounds.width)/3 {
            return 12
        }
        return 10
    }
    
}

