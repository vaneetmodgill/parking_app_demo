//
//  Utilities+Date.swift
//  Nehao
//
//  Created by Ajay Kumar on 13/06/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

extension(Utilities){
    static func covertEpochTimeToLocalTime(epochTime:Double)->String{
        // Convert NSString to NSTimeInterval
        let seconds = TimeInterval(epochTime)
        let epochDate:Date = Date(timeIntervalSince1970: seconds/1000.0)
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "h:mm a"
        return dateFormater.string(from: epochDate)
    }
    static func covertEpochTimeToLocalDate(epochTime:Double)->Date{
        // Convert NSString to NSTimeInterval
        let seconds = TimeInterval(epochTime)
        return Date(timeIntervalSince1970: seconds/1000.0)
    }
    static func covertEpochTimeToDate(epochTime:Double)->Date{
        // Convert NSString to NSTimeInterval
        let seconds = TimeInterval(epochTime)
        let epochDate:Date = Date(timeIntervalSince1970: seconds/1000.0)
        return epochDate
    }
    
    static func covertEpochTimeToDateString(epochTime:Double)->String{
        // Convert NSString to NSTimeInterval
        let seconds = TimeInterval(epochTime)
        let epochDate:Date = Date(timeIntervalSince1970: seconds/1000.0)
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd-MMM-yyyy h:mm a"
        
        return dateFormater.string(from: epochDate)
    }
    
    static func covertDateToDateString(date:Date)->String{
        // Convert NSString to NSTimeInterval
        let epochDate:Date = date
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd-MMM-yyyy h:mm a"
        
        return dateFormater.string(from: epochDate)
    }
    
    static func covertDateToDateString24Format(date:Date)->String{
        // Convert NSString to NSTimeInterval
        let epochDate:Date = date
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd-MMM-yyyy HH:mm"
        
        return dateFormater.string(from: epochDate)
    }
    
    static func covertToDateString(date:Date)->String{
        // Convert NSString to NSTimeInterval
        let epochDate:Date = date
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd-MMM-yyyy"
        
        return dateFormater.string(from: epochDate)
    }
    
    static func covertEpochTimeToMeaningFullDate(epochTime:Double)->String{
        // Convert NSString to NSTimeInterval
        let seconds = TimeInterval(epochTime)
        let epochDate:Date = Date(timeIntervalSince1970: seconds/1000.0)
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "h:mm a"
        var datePrefix = ""
        if(NSCalendar.current.isDateInToday(epochDate)){
            datePrefix = NSLocalizedString("Today", comment: "") + " "
            return dateFormater.string(from: epochDate)
        }else if(NSCalendar.current.isDateInYesterday(epochDate)){
            datePrefix = NSLocalizedString("Yesterday", comment: "") + " "
                return datePrefix
        }else{
            dateFormater.dateFormat = "dd-MM-yyyy"
            
        }
        return dateFormater.string(from: epochDate)
    }
    static func convertUtcDateToEpochTime(date:Date)->Double{
//        let dateFormatter = DateFormatter()
//        dateFormatter.timeZone = TimeZone(abbreviation: "UTC") as TimeZone!
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let utcDate = date
        let unixTime = utcDate.timeIntervalSince1970
        
        return unixTime
    }
    static func convertEpochTimeToUtcDate(time: Double) -> String{
        let date = NSDate(timeIntervalSince1970: TimeInterval(time))
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.timeZone = TimeZone(abbreviation: "UTC") as TimeZone!
        dayTimePeriodFormatter.dateFormat = "dd-MM-yyyy"
        return dayTimePeriodFormatter.string(from: date as Date)
    }
    
    static func convertEpochTimeToUtcTime(time: Double) -> String{
        let date = NSDate(timeIntervalSince1970: TimeInterval(time))
        let dayTimePeriodFormatter = DateFormatter()
       // dayTimePeriodFormatter.timeZone = TimeZone(abbreviation: "UTC") as TimeZone!
        dayTimePeriodFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        return dayTimePeriodFormatter.string(from: date as Date)
    }
    
    static func convertEpochTimeToUtcDate(date: Date) -> String{
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.timeZone = TimeZone(abbreviation: "UTC") as TimeZone!
        dayTimePeriodFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return dayTimePeriodFormatter.string(from: date as Date)
    }
    
    static func convertUtcStringToDate(_ date: String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC") as TimeZone!
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        if let formattedDate = dateFormatter.date(from: date){
            return formattedDate
        }
        return Date()
    }
    
    static func convertStringToDate(_ date: String) -> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC") as TimeZone!
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        if let newDate = dateFormatter.date(from: date){
            return newDate
        }
        return nil
    }
    
    static func convertUtcStringToDate(_ date: String, format:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC") as TimeZone!
        dateFormatter.dateFormat = format
        if let formattedDate = dateFormatter.date(from: date){
            return formattedDate
        }
        return Date()
    }
    
    static func convertUtcDateToString(_ date: Date, _ format: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    
    static func convertEpochTimeToLocalDate(time: Double) -> Date{
        return NSDate(timeIntervalSince1970: time) as Date
    }
    
    
    static func convertDateFormater(_ dateString: String, format: String, fromFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") -> String {
        if dateString == "" {
            return ""
        }
       
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = fromFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC") as TimeZone!
        
        let dateTime = dateFormatter.date(from: dateString)
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        //formatter.locale = Locale(identifier: "en-IN") as Locale!
        dateFormatter.timeZone = TimeZone.current
        formatter.dateFormat = format
        
        var dateTimeString =  ""
        if dateTime != nil {
            dateTimeString = formatter.string(from: dateTime!)
        } else {
            dateTimeString = "NA"
        }
        return dateTimeString
    }

}
struct DateComponentUnitFormatter {
    
    private struct DateComponentUnitFormat {
        let unit: Calendar.Component
        
        let singularUnit: String
        let pluralUnit: String
        
        let futureSingular: String
        let pastSingular: String
    }
    
    private let formats: [DateComponentUnitFormat] = [
        
        DateComponentUnitFormat(unit: .year,
                                singularUnit: NSLocalizedString("year", comment: ""),
                                pluralUnit: NSLocalizedString("years", comment: ""),
                                futureSingular: NSLocalizedString("Next year", comment: ""),
                                pastSingular: NSLocalizedString("Last year", comment: "")),
        
        DateComponentUnitFormat(unit: .month,
                                singularUnit: NSLocalizedString("month", comment: ""),
                                pluralUnit: NSLocalizedString("months", comment: ""),
                                futureSingular: NSLocalizedString("Next month", comment: ""),
                                pastSingular: NSLocalizedString("Last month", comment: "")),
        
        DateComponentUnitFormat(unit: .weekOfYear,
                                singularUnit: NSLocalizedString("week", comment: ""),
                                pluralUnit: NSLocalizedString("weeks", comment: ""),
                                futureSingular: NSLocalizedString("Next week", comment: ""),
                                pastSingular: NSLocalizedString("Last week", comment: "")),
        
        DateComponentUnitFormat(unit: .day,
                                singularUnit: NSLocalizedString("day", comment: ""),
                                pluralUnit: NSLocalizedString("days", comment: ""),
                                futureSingular: NSLocalizedString("Tomorrow", comment: ""),
                                pastSingular: NSLocalizedString("Yesterday", comment: "")),
        
        DateComponentUnitFormat(unit: .hour,
                                singularUnit: NSLocalizedString("hour", comment: ""),
                                pluralUnit: NSLocalizedString("hours", comment: ""),
                                futureSingular: NSLocalizedString("In an hour", comment: ""),
                                pastSingular: NSLocalizedString("An hour ago", comment: "")),
        
        DateComponentUnitFormat(unit: .minute,
                                singularUnit: NSLocalizedString("minute", comment: ""),
                                pluralUnit: NSLocalizedString("minutes", comment: ""),
                                futureSingular: NSLocalizedString("In a minute", comment: ""),
                                pastSingular: NSLocalizedString("A minute ago", comment: "")),
        
        DateComponentUnitFormat(unit: .second,
                                singularUnit: NSLocalizedString("second", comment: ""),
                                pluralUnit: NSLocalizedString("seconds", comment: ""),
                                futureSingular: NSLocalizedString("Just now", comment: ""),
                                pastSingular: NSLocalizedString("Just now", comment: "")),
        
        ]
    
    func string(forDateComponents dateComponents: DateComponents, useNumericDates: Bool) -> String {
        for format in self.formats {
            let unitValue: Int
            
            switch format.unit {
            case .year:
                unitValue = dateComponents.year ?? 0
            case .month:
                unitValue = dateComponents.month ?? 0
            case .weekOfYear:
                unitValue = dateComponents.weekOfYear ?? 0
            case .day:
                unitValue = dateComponents.day ?? 0
            case .hour:
                unitValue = dateComponents.hour ?? 0
            case .minute:
                unitValue = dateComponents.minute ?? 0
            case .second:
                unitValue = dateComponents.second ?? 0
            default:
                assertionFailure("Date does not have requried components")
                return ""
            }
            
            switch unitValue {
            case 2 ..< Int.max:
                return "\(unitValue) \(format.pluralUnit) " + NSLocalizedString("ago", comment: "")
            case 1:
                return useNumericDates ? "\(unitValue) \(format.singularUnit) " + NSLocalizedString("ago", comment: "") : "\(format.pastSingular)"
            case -1:
                return "Just now"
                return useNumericDates ? "In \(-unitValue) \(format.singularUnit)" : format.futureSingular
            case Int.min ..< -1:
                return "Just now"
                return "In \(-unitValue) \(format.pluralUnit)"
            default:
                break
            }
        }
        
        return "Just now"
    }
}

extension Date {
    
    func timeAgoSinceNow(useNumericDates: Bool = false) -> String {
        
        let calendar = Calendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date()
        let components = calendar.dateComponents(unitFlags, from: self, to: now)
        
        let formatter = DateComponentUnitFormatter()
        return formatter.string(forDateComponents: components, useNumericDates: useNumericDates)
    }
}
