//
//  Utilities+Keyboard.swift
//  Nehao
//
//  Created by Ajay Kumar on 20/06/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//
import UIKit
import Foundation
extension(Utilities){
    static func hideKeyboard() {
        UIApplication.shared.keyWindow?.endEditing(true)
    }
}
