//
//  Utilities+Video.swift
//  Nehao
//
//  Created by Tarsem on 05/10/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//
import UIKit
import AVFoundation
extension(Utilities){
    static func compressVideo(inputURL:URL, qulaity: String? = AVAssetExportPreset960x540,  format: String? = AVFileType.mp4.rawValue, callBack: @escaping(_ status:Bool) -> Void){
        
        let finaloutputURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mp4")
        compressVideo(inputURL: inputURL, outputURL: finaloutputURL, qulaity: qulaity!, format: format!,handler: { (exportSession) in
            guard let session = exportSession else {
                callBack(false)
                return
            }
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                print(session.progress)
                break
            case .completed:
                moveFile(from: finaloutputURL, to: inputURL, callBack: { status in
                    callBack(status)
                })
            case .failed:
                callBack(false)
                break
            case .cancelled:
                callBack(false)
                break
            }
        })
    }
    private static func compressVideo(inputURL: URL, outputURL: URL,qulaity: String, format:String, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: qulaity) else {
            handler(nil)
            return
        }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType(rawValue: format)
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    private static func moveFile(from:URL, to:URL, callBack: @escaping(_ status:Bool) -> Void) {
        guard FileManager.default.fileExists(atPath: from.path) else {
            callBack(false)
            return
        }
        deletFile(path: to.path, callBack: { status in  
            if status {
                do{
                    try FileManager.default.moveItem(at: from, to: to)
                    callBack(true)
                }catch{
                    callBack(false)
                }
            }else{
                callBack(false)
            }
        })
    }
    private static func deletFile(path:String, callBack: @escaping(_ status:Bool) -> Void){
        guard FileManager.default.fileExists(atPath: path) else {
            callBack(true)
            return
        }
        do {
            try FileManager.default.removeItem(atPath: path)
            callBack(true)
        }catch{
            callBack(false)
        }
    }
}
