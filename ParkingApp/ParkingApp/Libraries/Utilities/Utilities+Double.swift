//
//  Utilities+Double.swift
//  Nehao
//
//  Created by Yapapp on 07/03/18.
//  Copyright © 2018 Amit Tripathi. All rights reserved.
//

import Foundation

extension Double {
    /// Rounds the double to decimal places value
    func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor
    }
}
