//
//  AlertCentral.swift
//  Nehao
//
//  Created by Divakar on 12/13/16.
//  Copyright © 2016 Amit Tripathi. All rights reserved.
//
import UIKit
import Foundation
//import MBProgressHUD
struct AlertCentral {
    static func displayAlertForAPNSettings(){
        let settingsButton = NSLocalizedString("Settings", comment: "")
        let cancelButton = NSLocalizedString("Cancel", comment: "")
        let title = NSLocalizedString("Access to push notifications not available", comment: "")
        let message = NSLocalizedString("Please enable access to push notifications in order to use this feature.", comment: "")
        let goToSettingsAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        goToSettingsAlert.addAction(UIAlertAction(title: settingsButton, style: .destructive, handler: { (action: UIAlertAction) in
            DispatchQueue.main.async {
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    })
                }
            }
        }))
        goToSettingsAlert.addAction(UIAlertAction(title: cancelButton, style: .cancel, handler: nil))
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow!.rootViewController!.present(goToSettingsAlert, animated: false, completion: nil)
        }
        
    }
    static func displayAlert(title:String, message:String, style:UIAlertController.Style, actionButtonTitle:[String],completionHandler:@escaping (UIAlertAction)->Void,controller:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        alert.addAction(UIAlertAction(title: actionButtonTitle[0], style: UIAlertAction.Style.default, handler: completionHandler))
        alert.addAction(UIAlertAction(title:  actionButtonTitle[1], style: UIAlertAction.Style.default, handler: nil))
        DispatchQueue.main.async {
            controller.present(alert, animated: false, completion: nil)
        }
        
    }
    static func displayOkAlert(title:String, message:String, style:UIAlertController.Style, actionButtonTitle:String,completionHandler:@escaping (UIAlertAction)->Void,controller:UIViewController){
        if let presented = controller.presentedViewController {
            presented.removeFromParent()
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        alert.addAction(UIAlertAction(title: actionButtonTitle, style: UIAlertAction.Style.default, handler: completionHandler))
        DispatchQueue.main.async {
            controller.present(alert, animated: false, completion: nil)
        }
    }
    
    static func displayOkAlertOnWindow(withTitle title: String, message:String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil)
        alertController.addAction(cancelAction)
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow!.rootViewController!.present(alertController, animated: false, completion: nil)
        }
    }
    static func displayOkHudAlertOnWindow(message:String, toastPosition: ToastPosition = ToastPosition.bottom)
    {
        
        UIApplication.shared.keyWindow!.rootViewController!.view.makeToast(message, duration: 2.0, position: toastPosition, title: nil, image: nil, style: ToastManager.shared.style) { (completion) in
            
        }
        //        self.displayAlertView(title: "WedMe", message:message, style: .alert, actionButtonTitle: [NSLocalizedString("OK", comment: "")]) { (action) in
        //        }
        
        //
        //        let tost = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
        //        tost.mode = MBProgressHUDMode.text
        //        tost.labelText = message
        //        tost.margin = 10
        //        tost.label.textColor = .white
        //        tost.bezelView.color = UIColor.black.withAlphaComponent(0.7)
        //        tost.removeFromSuperViewOnHide = true
        //        tost.hide(true, afterDelay: 2)
    }
    
    
    static func displayAlertView(title:String, message:String, style:UIAlertController.Style, actionButtonTitle:[String],completionHandler:@escaping (UIAlertAction)->Void){
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        alert.addAction(UIAlertAction(title: actionButtonTitle[0], style: UIAlertAction.Style.default, handler: completionHandler))
        if actionButtonTitle.count > 1 {
            alert.addAction(UIAlertAction(title:  actionButtonTitle[1], style: UIAlertAction.Style.default, handler: nil))
        }
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow!.rootViewController!.present(alert, animated: false, completion: nil)
        }
    }
    
    static func displayAlertViewWithTwoButton(title:String, message:String, style:UIAlertController.Style, actionButtonTitle:[String],completionHandler:[(UIAlertAction)->Void]){
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        alert.addAction(UIAlertAction(title: actionButtonTitle[0], style: UIAlertAction.Style.default, handler: completionHandler[0]))
        if actionButtonTitle.count > 1 {
            alert.addAction(UIAlertAction(title:  actionButtonTitle[1], style: UIAlertAction.Style.default, handler: completionHandler[1]))
        }
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow!.rootViewController!.present(alert, animated: false, completion: nil)
        }
    }
    
    static func displayTwoOptionAlert(title:String, message:String, style:UIAlertController.Style, actionButtonTitle:[String],completionHandler:[(UIAlertAction)->Void],controller:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: style)
        alert.addAction(UIAlertAction(title: actionButtonTitle[0], style: UIAlertAction.Style.default, handler: completionHandler[0]))
        if actionButtonTitle.count > 1 {
            alert.addAction(UIAlertAction(title:  actionButtonTitle[1], style: UIAlertAction.Style.default, handler: completionHandler[1]))
        }
        controller.present(alert, animated: true, completion: nil)
        
    }
    
    
    static func showAlertWithTitle(with title:String) {
        let alertController = UIAlertController(title: NSLocalizedString("WedMe", comment: "") , message: title, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil)
        alertController.addAction(cancelAction)        
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow!.rootViewController!.present(alertController, animated: false, completion: nil)
        }
    }
    
    static func showViewController(on topVc:UIViewController){
        var vc = UIApplication.shared.keyWindow!.rootViewController!
        while (vc.presentedViewController != nil) {
            vc = vc.presentedViewController!
        }
        DispatchQueue.main.async {
            vc.present(topVc, animated: true, completion: nil)
        }
    }
    static func dismissAllPresentedViews(){
        let vc = UIApplication.shared.keyWindow!.rootViewController!
        if(vc.presentedViewController != nil) {
            vc.dismiss(animated: false, completion: {
                if(vc.presentedViewController != nil) {
                    vc.dismiss(animated: false, completion: {
                        if(vc.presentedViewController != nil) {
                            vc.dismiss(animated: false, completion: {
                            })
                        }
                    })
                }
            })
        }
    }
    static func showSettingsAlertView(withTitle title: String,body:String,requiredCancelButton:Bool,controller:UIViewController = UIApplication.shared.keyWindow!.rootViewController!) {
        DispatchQueue.main.async(execute: {() -> Void in
            let alertController = UIAlertController(title: title, message: body, preferredStyle: .actionSheet)
            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
                controller.becomeFirstResponder()
            })
            let settingsAction = UIAlertAction(title: NSLocalizedString("Setting", comment: ""), style: .default, handler: {(_ action: UIAlertAction) -> Void in
                controller.becomeFirstResponder()
                let appSettings = URL(string: UIApplication.openSettingsURLString)!
                UIApplication.shared.open(appSettings, options: [:], completionHandler: { (granted :Bool) in
                    if granted {
                    } else {
                        
                    }
                })
            })
            if requiredCancelButton {
                alertController.addAction(cancelAction)
            }
            alertController.addAction(settingsAction)
            controller.resignFirstResponder()
            DispatchQueue.main.async {
                controller.present(alertController, animated: false, completion: nil)
            }
        })
    }
    
    static func showInternetAlert() {
        let alertController = UIAlertController(title: NSLocalizedString(" The Parker", comment: "") , message: NSLocalizedString("NoInternetAlert", comment:""), preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil)
        alertController.addAction(cancelAction)
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow!.rootViewController!.present(alertController, animated: false, completion: nil)
        }
    }
    
}
