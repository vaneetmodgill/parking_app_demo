//
//  Utilities+Array.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 01/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation

extension(Utilities){
    
    static func isArrayEmpty(_ array:Array<Any>?) -> Bool{
        if (array?.count == nil ? -1:(array?.count)!) > 0{
            return false
        }
        return true
    }
}
