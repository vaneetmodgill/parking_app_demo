//
//  Utilities+ViewSize.swift
//  Nehao
//
//  Created by Divakar on 6/6/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//
import UIKit
import Foundation
extension (Utilities){
    
    static func heightForView(with viewType:FontViewType,withText text:String ,with font:UIFont,withWidth width:CGFloat)->CGFloat{
        switch viewType {
        case .textView:
           return heightForTextView(withText: text, with: font, withWidth: width)
        case .label:
          return  heightForLabel(withText: text, with: font, withWidth: width)
        default :
          return 0
        }
    }
    private static func heightForTextView(withText text: String, with font: UIFont, withWidth width: CGFloat) -> CGFloat {
        let textView = UITextView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: width, height: CGFloat(0)))
        textView.textContainer.lineBreakMode = .byWordWrapping
        textView.text = text
        textView.font = font
        let height: CGFloat = CGFloat(ceilf(Float(textView.sizeThatFits(textView.frame.size).height)))
        return height
    }
     private static func heightForLabel(withText text: String, with font: UIFont, withWidth width: CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
   
}
