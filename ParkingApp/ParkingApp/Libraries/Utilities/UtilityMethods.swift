//
//  UtilityMethods.swift
//  Nehao
//
//  Created by Divakar on 12/7/16.
//  Copyright © 2016 Amit Tripathi. All rights reserved.
//
import UIKit
import Foundation
import AVFoundation
struct UtiltityMethods {
    static func setCorner(radius rad:CGFloat,forView view:UIView){
        view.layer.cornerRadius = rad
        view.layer.masksToBounds = true
    }
    
   static func colorWithHexString (hex:String) -> UIColor {
    var hexString:String = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased()
        if (hexString.hasPrefix("#")) {
            hexString = (hexString as NSString).substring(from: 1)
        }
        if (hexString.count != 6) {
            return UIColor.gray
        }
        let rString = (hexString as NSString).substring(to: 2)
        let gString = ((hexString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((hexString as NSString).substring(from: 4) as NSString).substring(to: 2)
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
    
   static func topPresentedViewController() -> UIViewController {
        var vc = UIApplication.shared.keyWindow!.rootViewController!
        while nil != vc.presentedViewController {
            vc = vc.presentedViewController!
        }
        return vc
    }
    static func areSameDays(_ date1: DateComponents, withDate date2: DateComponents) -> Bool {
        let result: Bool? = date1.day == date2.day && date1.month == date2.month && date1.year == date2.year && date1.era == date2.era
        return result!
    }
    static func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    //****** Jan 19
    
//    static func compressedImage(image: UIImage?) -> UIImage? {
//        let imageData1 = UIImagePNGRepresentation(image!)
//        let image = UIImage(data: imageData1!)
//        // Reducing file size to a 10th
//        var actualHeight: CGFloat = image!.size.height
//        var actualWidth: CGFloat = image!.size.width
//        let maxHeight: CGFloat = 1136.0
//        let maxWidth: CGFloat = 640.0
//        var imgRatio: CGFloat = actualWidth / actualHeight
//        let maxRatio: CGFloat = maxWidth / maxHeight
//        let compressionQuality: CGFloat = 0
//        
//        if (actualHeight > maxHeight || actualWidth > maxWidth) {
//            if (imgRatio < maxRatio) {
//                // adjust width according to maxHeight
//                imgRatio = maxHeight / actualHeight;
//                actualWidth = imgRatio * actualWidth;
//                actualHeight = maxHeight;
//            }
//            else if (imgRatio > maxRatio) {
//                // adjust height according to maxWidth
//                imgRatio = maxWidth / actualWidth;
//                actualHeight = imgRatio * actualHeight;
//                actualWidth = maxWidth;
//            }
//            else {
//                actualHeight = maxHeight;
//                actualWidth = maxWidth;
//            }
//        }
//        
//        let rect = CGRect(x: 0, y: 0, width: actualWidth, height: actualHeight)
//        UIGraphicsBeginImageContext(rect.size)
//        image!.draw(in: rect)
//        let img = UIGraphicsGetImageFromCurrentImageContext();
//        let imageData = UIImageJPEGRepresentation(img!, compressionQuality);
//        UIGraphicsEndImageContext();
//        return UIImage(data: imageData!)
//    }
//    
//    static func compressImageUsingScale(image:UIImage)->UIImage?{
//        let imageData:NSData?
//        imageData = NSData.init(data: UIImageJPEGRepresentation(image, 0)!)
//        
//        let scale:CGFloat = CGFloat((10*1024))/CGFloat((imageData?.length)!)
//        
//        let small_image = UIImage.init(cgImage: image.cgImage!, scale: scale, orientation: image.imageOrientation)
//        
//        let compressedImageData = UIImageJPEGRepresentation(small_image, scale*1.00)
//        let compressedImage = UIImage(data: compressedImageData!)
//        YALog.print(compressedImage?.kilobytesSize)
//        return compressedImage
//    }
    
    static func hideKeyboard() {
        UIApplication.shared.keyWindow?.endEditing(true)
    }
    
    static func appVersionDetails() -> String {
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let versionname = version?.appending(".")
        let build = Bundle.main.infoDictionary!["CFBundleVersion"] as! String
        return (versionname?.appending(build))!
    }
    
    static func fetchingDeviceModelName() -> String {
        let model = UIDevice.current.model
        return model
    }
    static func generateThumbImage(url : URL) -> UIImage?{
        let asset = AVAsset(url: url)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMake(value: 1, timescale: 30)
        let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
        guard let cgImage = img else { return nil }
        let frameImg    = UIImage(cgImage: cgImage)
        return frameImg
    }
    
    static func validateVPAAddress(_ text: String) -> Bool {
        let vpaRegex = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+"
        let vpaTest = NSPredicate(format: "SELF MATCHES %@", vpaRegex)
        return vpaTest.evaluate(with: text)
    }
    
    static func formatAttributeName(name:String, vpa:String, color1:UIColor, color2:UIColor) -> NSAttributedString {
        
        let attr = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17.0), NSAttributedString.Key.foregroundColor: color1]
        let attr1 = [NSAttributedString.Key.foregroundColor: color2, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15.0)] as [NSAttributedString.Key : Any]
        
        var formatedString = NSMutableAttributedString()
        if name == "" {
            formatedString = NSMutableAttributedString(string:vpa, attributes:attr)
            formatedString.addAttributes(attr, range: NSRange(location:0,length:formatedString.length))
        }
        else {
            
            let formatedName = NSMutableAttributedString(string:name, attributes:attr)
            formatedName.addAttributes(attr, range: NSRange(location:0,length:formatedName.length))
            
            let formatedVpa = NSMutableAttributedString(string:"\n\(vpa)", attributes:attr1)
            formatedVpa.addAttributes(attr1, range: NSRange(location:0,length:formatedVpa.length))
            
            
            formatedString.append(formatedName)
            formatedString.append(formatedVpa)
        }
        
        return formatedString
    }
    
}
