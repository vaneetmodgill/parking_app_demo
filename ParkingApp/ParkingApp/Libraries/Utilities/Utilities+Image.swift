//
//  Utilities+Array.swift
//  Nehao
//
//  Created by Tarsem on 15/05/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//
import UIKit
extension(Utilities){
    
    enum ImageQuality:CGFloat {
        case iq_50 = 50, iq_100 = 100 ,iq_320 = 320, iq_480 = 480 , iq_640 = 640 , iq_720 = 720 , iq_1024 = 1024
    }
    
    static func scaleImage(sourceImage: UIImage, imageQuality:ImageQuality) -> UIImage{
        let finalSize:CGSize = getNewsize(sourceSize: sourceImage.size, targetSize: imageQuality.rawValue)
        UIGraphicsBeginImageContextWithOptions(finalSize, false, 0)
        sourceImage.draw(in: CGRect(origin: .zero, size: finalSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    static func getNewsize(sourceSize:CGSize, targetSize:CGFloat) -> CGSize{
        let width: CGFloat = sourceSize.width
        let height: CGFloat = sourceSize.height
        var scaleFactor: CGFloat = 0.0
        var scaledWidth: CGFloat = targetSize
        var scaledHeight: CGFloat = targetSize
        
        let widthFactor: CGFloat = targetSize / width
        let heightFactor: CGFloat = targetSize / height
        
        if widthFactor > heightFactor {
            scaleFactor = widthFactor
        }
        else {
            scaleFactor = heightFactor
        }
        scaledWidth = width * scaleFactor
        scaledHeight = height * scaleFactor
        return CGSize(width: scaledWidth, height: scaledHeight)
    }
    
    static func generateImage(view: UIView)-> UIImage{
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    static func generateImage(image: UIImage, text:String, color: UIColor = .black, font: UIFont = UIFont(name: "Helvetica Bold", size: 12)!, point: CGPoint = CGPoint.init(x: 20, y: 20)) -> UIImage{
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes = [
            NSAttributedString.Key.font.rawValue: font,
            NSAttributedString.Key.foregroundColor: color,
            ] as! [NSAttributedString.Key : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        
        let rect = CGRect(origin: point, size: image.size)
        text.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }

    
}
extension UIImage {
    func imageWithColor(color: UIColor) -> UIImage? {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
