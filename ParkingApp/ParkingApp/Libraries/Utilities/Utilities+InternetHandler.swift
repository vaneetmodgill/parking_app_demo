//
//  Utilities+InternetHandler.swift
//  WedMe
//
//  Created by Tina Gupta on 09/04/18.
//  Copyright © 2018 YapApp India Pvt. Ltd. All rights reserved.
//

import Foundation

extension(Utilities){
    static func checkInternetAndShowAlert() -> Bool{
        if AppCacheData.sharedInstance.isNetworkAvailable{
            return true
        }else{
            AlertCentral.showInternetAlert()
            return false
        }
        
    }
    
}
