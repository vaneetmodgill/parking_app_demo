//
//  Utilities+StatusBar.swift
//  Nehao
//
//  Created by VaneetMoudgill on 16/05/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
import UIKit
extension(Utilities){
    static func handleStatusbarForIncomingPhoneCall(view:UIView)->UIView{
        view.autoresizesSubviews = true
        view.autoresizingMask =  UIView.AutoresizingMask.flexibleTopMargin
        view.autoresizingMask = UIView.AutoresizingMask.flexibleHeight;
        return view
    }
    
}
