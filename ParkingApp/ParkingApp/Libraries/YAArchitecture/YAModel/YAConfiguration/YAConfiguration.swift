//
//  YAConfiguration.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 21/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit
 
public class YAConfiguration {
    
    static func initConfigurationWithClient(client:YAConfiguration) -> YAConfiguration{
        return client
    }
    
    func webServerHost() -> String? {
        fatalError("This function should only be called by its concrete class")
    }

}



