//
//  YAWebServiceAPI.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 21/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

open class YAWebServiceAPI: YAServiceAPI {
    
    open static func initServiceAPIWithClient(client:YAWebServiceAPI) -> YAWebServiceAPI{
        return client
    }
    
    open func call(request:YAWebServiceRequest, delegate:YAServiceAPIDelegate) {
        fatalError("This function should only be called by  its concrete class")

    }
}
