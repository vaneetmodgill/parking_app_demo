//
//  YAWebServiceRequest.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 21/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit
    
open class YAWebServiceRequest: NSObject {
    
    public enum MethodType {
        case GET
        case POST
        case PUT
        case DELETE
    }
    
    public enum URLEncodingType{
        case FORM
        case QUERY
        case JSONENCODING
        case FileUpload
    }
    
    open var selectedMethodType:MethodType = .GET
    open var selectedURLEncodingType:URLEncodingType = .FORM
    open var url:String = ""
    open var parameters:[String:Any]?
    open var headers:[String:String]?
    open var timeOutSession = 60
    
    static func initWithClient(client:YAWebServiceRequest) -> YAWebServiceRequest{
        return client
    }
    
    open func createRequest(hostURL:String, methodType:MethodType, urlEncodingType:URLEncodingType, requestTimeOut:Int, parameters:[String:AnyObject]?, headers:[String:String]?) -> YAWebServiceRequest {
        fatalError("This function should can only be called by its concrete class")
    }
}
