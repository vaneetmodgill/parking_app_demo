//
//  YAWebServiceManager.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 21/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

public protocol YAWebServiceManagerProtocol: class {
    func webserviceIsBodyParameterRequired()->Bool
    
    func webserviceBodyParameters(data:AnyObject?)->[String:AnyObject]?
    
    func webserviceIsHeaderParameterRequired()->Bool
    
    func webserviceHeaderParameters(data:AnyObject?)->[String:AnyObject]?
    
    func webserviceAPI()->String?
    
    func webServerHost()->String?
    
    func webServiceIsAccessTokenRequired()->Bool
    
    func requestTimeOut()->Int?
        
    func webServiceAPIClient()->YAWebServiceAPI?
    
    func webServiceMethodType()->YAWebServiceRequest.MethodType
    
    func webServiceEncodingType()->YAWebServiceRequest.URLEncodingType
    
}

open class YAWebServiceManager: YAServiceManager, YAServiceAPIDelegate {
    open weak var serviceProtocol:YAWebServiceManagerProtocol?
    open var bodyParameters:[String:AnyObject]?
    open var headerParameters:[String:AnyObject]?
    open var API:String?
    open var hostUrlString:String?
    open var accessToken:String?
    

    static func initWithClient(client:YAWebServiceManager) -> YAWebServiceManager{
        return client
    }
    
    func callWebService(data:AnyObject?, accessToken:String?, delegate:YAServiceManagerDelegate?){
        fatalError("This function should can only be called by its concrete class")
    }
    
    //MARK: - Service API Delegate Method(s)
    public func didCompleteAPI(sender: YAServiceAPI) {
        fatalError("This function should can only be called by its concrete class")
    }
    
    public func failedToCompleteAPI(sender: YAServiceAPI) {
        fatalError("This function should can only be called by its concrete class")
    }
    
}
