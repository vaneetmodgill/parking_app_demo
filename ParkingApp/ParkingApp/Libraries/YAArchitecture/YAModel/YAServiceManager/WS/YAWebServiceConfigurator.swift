//
//  YAWebServiceConfigurator.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 08/02/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

open class YAWebServiceConfigurator:NSObject{
    
    static func initConfiguratorWithClient(client:YAWebServiceConfigurator) -> YAWebServiceConfigurator{
        return client
    }
    
    func call(data:AnyObject?, accessToken:String?, delegate:YAServiceManagerDelegate?, manager:YAWebServiceManager){
        fatalError("This function should only be called by its concrete class")
    }
    
    func genericAccessTokenHeader(manager:YAWebServiceManager)->[String:AnyObject]?{
        fatalError("This function should only be called by its concrete class")
    }
    
    func genericAPICLient()->YAWebServiceAPI{
        fatalError("This function should only be called by its concrete class")
    }
    
    func convertJSONString(string:String)->[String: Any]?{
        fatalError("This function should only be called by its concrete class")
    }
    
    func convertJSONDictionary(dictionary:[String: Any])->String?{
        fatalError("This function should only be called by its concrete class")
    }
    
    func convertJSONArray(string:String)->[Any]?{
        fatalError("This function should only be called by its concrete class")
    }

    func isGenericAuthenticationValid(response:[String:Any]?)->Bool{
        fatalError("This function should only be called by its concrete class")
    }
    
    func isGenericResponseValid(response:[String:Any]?)->Bool{
        fatalError("This function should only be called by its concrete class")
    }
    
    func getErrorResponse(response:[String:Any]?)->YAError{
        fatalError("This function should only be called by its concrete class")
    }
    
}
