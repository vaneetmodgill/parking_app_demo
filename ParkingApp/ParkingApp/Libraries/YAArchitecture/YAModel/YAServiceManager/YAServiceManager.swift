//
//  YAServiceManager.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 21/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

public protocol YAServiceManagerDelegate: class {
    func didCompleteService(sender: YAServiceManager)
    func failedToCompleteService(sender: YAServiceManager)
}

open class YAServiceManager: NSObject {
    open var result:AnyObject?
    open var request:AnyObject?
    open var error:YAError?
    open weak var delegate:YAServiceManagerDelegate?
}
