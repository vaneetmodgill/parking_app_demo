//
//  YADBServiceConfigurator.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 09/02/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit

class YADBServiceConfigurator: NSObject {
    
    static func initConfiguratorWithClient(client:YADBServiceConfigurator) -> YADBServiceConfigurator{
        return client
    }
    
    func call(data:AnyObject?, methodType: YADBServiceRequest.MethodType, queryString:String?, sortQueryString:String?, sortType:YADBServiceRequest.SortType?, manager:YADBServiceManager, completionHandler: @escaping(_ result:AnyObject?,_ error:YAError?)->Void){
        fatalError("This function should only be called by its concrete class")
    }
}
