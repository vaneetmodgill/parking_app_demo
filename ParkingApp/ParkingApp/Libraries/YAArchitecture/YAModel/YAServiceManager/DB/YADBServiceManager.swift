//
//  YADBServiceManager.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 23/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit

public protocol YADBServiceManagerProtocol: class {
    func dbServiceAPIClient()->YADBServiceAPI
    func dbServiceStorageTypeEncoding()->YADBServiceRequest.StorageTypeEncoding
}

open class YADBServiceManager: YAServiceManager, YAServiceAPIDelegate {
    
    open weak var serviceProtocol:YADBServiceManagerProtocol?
    open var queryString:String?
    open var sortQueryString:String?
    open var sortType:YADBServiceRequest.SortType?
    
    static func initWithClient(client:YADBServiceManager) -> YADBServiceManager{
        return client
    }
    //accessToken:String?
    func callDBService(data:AnyObject?, methodType:YADBServiceRequest.MethodType, queryString:String?, sortQueryString:String?, sortType:YADBServiceRequest.SortType?, completionHandler:@escaping(_ result:AnyObject?,_ error:YAError?)->Void){
        fatalError("This function should can only be called by its concrete class")
    }
    
    //MARK: - Service API Delegate Method(s)
    public func didCompleteAPI(sender: YAServiceAPI) {
        fatalError("This function should can only be called by its concrete class")
    }
    
    public func failedToCompleteAPI(sender: YAServiceAPI) {
        fatalError("This function should can only be called by its concrete class")
    }
}
