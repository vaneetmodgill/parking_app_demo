//
//  YAViewController.swift
//  YapAppCallingModule
//
//  Created by Abhimanu Jindal on 21/11/16.
//  Copyright © 2016 YapApp India Pvt. Ltd. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


open class YAViewController: UIViewController {
     
    override open func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override open func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view = Utilities.handleStatusbarForIncomingPhoneCall(view: self.view)
    }

    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- IQKEYBOARDMANAGER METHOD(S)
    open func activateIQKeyBoardManager(){
        IQKeyboardManager.shared.enable = true
    }
    
    open func deActivateIQKeyBoardManager(){
        IQKeyboardManager.shared.enable = false
    }
    open func displayAlert(title:String, message:String, actionButtonTitle:String, controller:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actionButtonTitle, style: UIAlertAction.Style.default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
//    open func showWebServiceError(error:TDError, controller:UIViewController){
//        var message = error.description ?? Constant.NetworkError.generalMessage
//        if error.error.localizedDescription == TDWebServiceError.networkNotReachable.localizedDescription {
//            message = Constant.NetworkError.networkNotReachable
//        }
//        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//        controller.present(alert, animated: true, completion: nil)
//    }
    
    open func addBlurBackgroundView(){
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        self.view.sendSubviewToBack(blurEffectView)
    }

    
}
