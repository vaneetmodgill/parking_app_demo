//
//  YAAPNSManager.swift
//  Nehao
//
//  Created by Nallabothula Mahesh on 01/12/16.
//  Copyright © 2016 Amit Tripathi. All rights reserved.
//

import UIKit
import UserNotifications
import PushKit

protocol YAAPNSManagerDelegate:class{
    func didReceiveDeviceToken(token:String)
    func didReceiveVoipToken(token:String)
    func didReceiveVoipMessage(message:[AnyHashable : Any])
    func didReceiveLocalNotification(userInfo:[AnyHashable:Any])
}
class YAAPNSManager: NSObject, UNUserNotificationCenterDelegate, PKPushRegistryDelegate {
    
    enum ApnPermissionStatus{
        case denied, granted, notDetermined, unknown
    }
    
    //MARk:- Variables/Constant
    static let sharedInstance = YAAPNSManager()
    var apnPermissionStatus: ApnPermissionStatus = .unknown
    var deviceToken = ""
    var voipToken = ""
    weak var delegate:YAAPNSManagerDelegate?
    
    private override init(){
        super.init()
        let center = UNUserNotificationCenter.current()
        center.delegate = self
    }
    
   
    //MARK:- Public Method(s)
    func requestAuthorization(completionHandler:@escaping (Bool,Error?) -> ()) {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
            // Enable or disable features based on authorization.
            if granted{
                self.apnPermissionStatus = .granted
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
                self.setUpVoipNotification()
            }
            else{
                print("Error in accessing APNS \(String(describing: error?.localizedDescription))")
            }
            completionHandler(granted,error)
        }
    }
    
    func setUpDeviceToken(deviceToken:Data){
        let token = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        self.deviceToken = token
        self.delegate?.didReceiveDeviceToken(token: self.deviceToken)
    }
    
    //MARK:- Private Method(s)
    
    private func setUpVoipNotification(){
        let voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
        voipRegistry.desiredPushTypes = Set([PKPushType.voIP])
        voipRegistry.delegate = self;
    }
    
    //MARK:- Notification Center Delegate Method(s)
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
     self.delegate?.didReceiveLocalNotification(userInfo: response.notification.request.content.userInfo)
  

    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        // Delivers a notification to an app running in the foreground.
    }
    
    //MARK: - Pushkit Delegate Method(s)
    func pushRegistry(_ registry: PKPushRegistry, didUpdate credentials: PKPushCredentials, for type: PKPushType){
        let token = credentials.token.reduce("", {$0 + String(format: "%02X", $1)})
        self.voipToken = token
        self.delegate?.didReceiveVoipToken(token: self.voipToken)
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType){
        self.delegate?.didReceiveVoipMessage(message: payload.dictionaryPayload)
//        print("###Notification payload### ->\(payload.dictionaryPayload)")
        
        //CallPushServiceManager.sharedInstance.startHandlingEvents()
        //CallPushServiceManager.sharedInstance.executeEvent(message:payload.dictionaryPayload as? [String:AnyObject])
    }
    
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType){
        let data:Data? = registry.pushToken(for: type)
        var token = ""
        
        if data != nil{
            let data1 = data!
            token = data1.reduce("", {$0 + String(format: "%02X", $1)})
        }
        
        print("###Voip Token needs to be removed from server for sending any message to this token### ->" + token)
        
    }

}
