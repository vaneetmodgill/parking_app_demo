//
//  Location.swift
//  RaysApp
//
//  Created by Vaneet on 21/08/18.
//  Copyright © 2018 YapApp. All rights reserved.
//

import ObjectMapper
import Foundation

class LocationModel: Mappable{
     var address=""
     var locationLatitude = 0.0
     var locationLongitude =  0.0
     var city = ""
    var state = ""
    var pincode = ""
    var country = ""

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        address                 <- map["address"]
        locationLatitude        <- map["lat"]
        locationLongitude       <- map["lng"]
        city                    <- map["city"]
         state                    <- map["state"]
         pincode                    <- map["pincode"]
         country                    <- map["country"]
        
    }
    
    class func mapModelData(model:LocationModel)->String{
        return Mapper<LocationModel>().toJSONString(model)!
    }
    
    class func mapJsonData(jsonString:String)->LocationModel?{
        return Mapper<LocationModel>().map(JSONString: jsonString)
    }
    class func mapJsonDictionaryData(jsonDictionary:[String:AnyObject])->LocationModel? {
        return Mapper<LocationModel>().map(JSON:jsonDictionary)
    }
}

