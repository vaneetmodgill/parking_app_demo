//
//  GeolocationModel.swift
//  ParkingApp
//
//  Created by Tina Gupta on 18/07/18.
//  Copyright © 2018 YapApp. All rights reserved.
//
import Foundation
import FirebaseDatabase
import ObjectMapper
import CoreLocation


class GeolocationModel : Mappable{
    var location :CLLocation?
    var latitude: Double?
    var longitude:Double?
    var isAvailable:Bool = false
    var userId:String? = AppCacheData.sharedInstance.getUserId()

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
//        location <- map["location"]
        isAvailable <- map["isAvailable"]
        userId <- map["userId"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        let loc = CLLocation(latitude: latitude!, longitude: longitude!)
        location = loc

    }
    
    
    class func mapJsonDictionaryData(jsonDictionary:[String:AnyObject])->GeolocationModel? {
        return Mapper<GeolocationModel>().map(JSON:jsonDictionary)
    }
    
    func mapToDictionary() -> [String:AnyObject] {
        let json = Mapper<GeolocationModel>().toJSON(self)
        return json as [String : AnyObject]
    }
}


enum CustomDataModelGeolocation: YPDatabaseTarget {
    
    case getLocationData(forKey:String)
    case addLocationData(locationModel:GeolocationModel, forKey :String)
    case updateLocationData(locationModel:GeolocationModel, forKey :String)
    case updateAvailabilityData(lcoationModel:GeolocationModel, forKey:String)
    case getUserParkingData(userId:String, forKey :String)
    var baseReference: DatabaseReference {
        return Database.database().reference().child("Locations")
    }
    
    var path: String {
        switch self {
        case .addLocationData( _,let nodeKey):
            return nodeKey
        case .updateAvailabilityData( _,let nodeKey):
            return nodeKey
        case .getLocationData(let nodeKey):
            return nodeKey
        case .updateLocationData( _,let nodeKey):
            return nodeKey
        case .getUserParkingData(_,let key):
            return key
        }
    }
    
    var task: YPDatabaseTask {
        
        switch self {
        case .getLocationData(_):
            return .observeOnce(.value)
            
        case .addLocationData(let locationData,_):
            return .setValue(["latitude":locationData.latitude!, "longitude":locationData.longitude!, "createdAt": ServerValue.timestamp(),"isAvailable":locationData.isAvailable, "updatedAt": ServerValue.timestamp(),"userId":locationData.userId])
        case .updateAvailabilityData(let locationData,_):
            return .updateChildValues(["isAvailable":locationData.isAvailable, "createdAt": ServerValue.timestamp(), "updatedAt": ServerValue.timestamp(),"userId":locationData.userId])
            
        case .updateLocationData(let locationData,_):
            return .updateChildValues(["latitude":locationData.latitude!, "longitude":locationData.longitude!, "createdAt": ServerValue.timestamp(),"isAvailable":locationData.isAvailable, "updatedAt": ServerValue.timestamp(),"userId":locationData.userId])
            
        case .getUserParkingData(let userId, let nodeKey): 
            return .observeOnce(.value)
        }
    }
    
    var queries: [YPDatabaseQuery]?{
        switch self {
        case .getLocationData(let key):
            return nil
        case .getUserParkingData(let userId, let nodeKey):
            return nil

//            var q1 = YPDatabaseQuery.orderedByChild(key: "userId")
//            var q2 = YPDatabaseQuery.equalToValue(userId)
//            return [q1,q2]
        default:
            return nil
        }
    }
    
}
