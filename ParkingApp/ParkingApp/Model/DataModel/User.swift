//
//  User.swift
//  ParkingApp
//
//  Created by Vaneet on 07/08/18.
//  Copyright © 2018 YapApp. All rights reserved.
//

import Foundation
import ObjectMapper
import Firebase

class User: Mappable {
    var fullName: String     = ""
    var password: String      = ""
    var emailId: String      = ""
    var homeAddress: String      = ""
    var workAddress: String      = ""
    var phoneNumber: String      = ""
    var carType:String    = ""
    var homeAddressLocation:LocationModel?
    var workAddressLocation:LocationModel?
    var userId = ""
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        fullName         <- map["fullName"]
        password          <- map["password"]
        emailId         <- map["emailId"]
        homeAddress          <- map["homeAddress"]
        workAddress         <- map["workAddress"]
        phoneNumber          <- map["phoneNumber"]
           carType          <- map["carType"]
                homeAddressLocation          <- map["homeAddressLocation"]
                workAddressLocation          <- map["workAddressLocation"]
        userId  <- map["userId"]
        
    }
    
    class func mapJsonDictionaryData(jsonDictionary:[String:AnyObject])->User? {
        return Mapper<User>().map(JSON:jsonDictionary)
    }
    class func mapJsonData(jsonString:String)->User?{
        return Mapper<User>().map(JSONString: jsonString)
    }
    class func mapModelData(model:User)->String{
        return Mapper<User>().toJSONString(model)!
    }
    func mapToDictionary() -> [String:AnyObject] {
        let json = Mapper<User>().toJSON(self)
        return json as [String : AnyObject]
    }
    
}

enum CustomDataModelUser: YPDatabaseTarget {
    
    case signUpUser(user:User)
    case getUser(email:String)
    case updateUser(user:User)
    
    var baseReference: DatabaseReference {
        return  Database.database().reference().child("Users")
    }
    
    var path: String {
        switch self {
        case .signUpUser(let user):
            return user.userId
        case .getUser(_):
            return ""
        case .updateUser(let user):
              return user.userId
        }
    }
    
    var task: YPDatabaseTask {
        
        switch self {
        case .signUpUser(let user):
            return .setValue(user.mapToDictionary())
        case .getUser(_):
            return .observeOnce(.value)
        case .updateUser(let user):
            return .updateChildValues(user.mapToDictionary())
        }
    }
    
    var queries: [YPDatabaseQuery]?{
        switch self {
        case .signUpUser(_):
            return nil
        case .getUser(let email):
            let q1 = YPDatabaseQuery.orderedByChild(key: "emailId")
            let q2 = YPDatabaseQuery.equalToValue(email)
            return [q1,q2]
        case .updateUser(_):
            return nil
        }
    }
    
}
