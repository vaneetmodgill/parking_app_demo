//
//  ParkingDataModel.swift
//  ParkingApp
//
//  Created by Tina Gupta on 17/07/18.
//  Copyright © 2018 YapApp. All rights reserved.
//

import Foundation
import FirebaseDatabase
import ObjectMapper



class ParkingStatusModel : Mappable{
    var isAvailable = false
   var userId = 1234
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        isAvailable <- map["isAvailable"]
        userId <- map["userId"]
    }
    
    
    class func mapJsonDictionaryData(jsonDictionary:[String:AnyObject])->ParkingStatusModel? {
        return Mapper<ParkingStatusModel>().map(JSON:jsonDictionary)
    }
    
    func mapToDictionary() -> [String:AnyObject] {
        let json = Mapper<ParkingStatusModel>().toJSON(self)
        return json as [String : AnyObject]
    }
}


enum CustomDataModelParkingStatus: YPDatabaseTarget {
    
    case getParkingData(forKey:String)
    case addParkingData(parkingModel:ParkingStatusModel, forKey :String)
    case updateParkingData(parkingModel:ParkingStatusModel, forKey :String)
    
    var baseReference: DatabaseReference {
        return Database.database().reference().child("ParkingStatus")
    }
    
    var path: String {
        switch self {
        case .addParkingData(let parkingData,let nodeKey):
                return nodeKey
        case .getParkingData(let nodeKey):
            return nodeKey
        case .updateParkingData(let parkingData,let nodeKey):
            return nodeKey
        }
    }
    
    var task: YPDatabaseTask {
        
        switch self {
        case .getParkingData(let key):
            return .observeOnce(.value)
       
        case .addParkingData(let parkingData,let nodeKey):
            return .setValue(["isAvailable":parkingData.isAvailable, "createdAt": ServerValue.timestamp(), "updatedAt": ServerValue.timestamp()])
            
        case .updateParkingData(let parkingData,let nodeKey):
            return .updateChildValues(["isAvailable":parkingData.isAvailable, "createdAt": ServerValue.timestamp(), "updatedAt": ServerValue.timestamp()])
        }
    }
    
    var queries: [YPDatabaseQuery]?{
        switch self {
        case .getParkingData(let key):
            return nil
        default:
            return nil
        }
    }
    
}
