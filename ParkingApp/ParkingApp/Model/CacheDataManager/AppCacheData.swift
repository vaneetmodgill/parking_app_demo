//
//  AppCacheData.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 14/02/17.
//  Copyright 2017 Amit Tripathi. All rights reserved.
//
import UIKit


class AppCacheData: NSObject {
    
    static let sharedInstance = AppCacheData()
    var isNetworkAvailable :Bool = false
    var user = User()
    private override init() {
        super.init()
        
    }
  
    func getUserId() -> String {
       return self.user.emailId
    }
    
    
}

