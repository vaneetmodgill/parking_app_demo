//
//  AlertMessages.swift
//  WedMe
//
//  Created by yapapp on 4/19/18.
//  Copyright © 2018 YapApp India Pvt. Ltd. All rights reserved.
//

import Foundation
enum AlertMessage:String {
    case additionalExpAdded = "Additional expenses added successfully"
    case additionalExpUpdated = "Additional expenses updated successfully"
    case profileUpdated = "Profile updated successfully"
    case enterEmail = "Please input your email:"
    case enterBudget = "Please enter your budget."
    case addBudgetSuccess = "Budget added successfully."
    case addBudgetFailure = "Unable to add budget. Please try again."
}

enum AlertTitle:String {
    case email = "Email?"
}
