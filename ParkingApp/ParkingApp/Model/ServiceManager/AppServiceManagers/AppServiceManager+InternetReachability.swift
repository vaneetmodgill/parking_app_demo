//
//  AppServiceManager_InternetReachability.swift
//  WedMe
//
//  Created by Tina Gupta on 09/04/18.
//  Copyright © 2018 YapApp India Pvt. Ltd. All rights reserved.
//

import Foundation
extension(AppServiceManager):NetworkReachabilityNotifierDelegate{
    
    func initiateNetworkReachabilityObserver(){
        networkReachabilityNotifier.initateNotification()
    }
    
    //MARK:- DELEGATE METHOD(S)
    
    func networkReachable(isReachable: Bool, rechableType: NetworkReachabilityNotifier.ReachableType) {
        AppCacheData.sharedInstance.isNetworkAvailable = isReachable
    }
}
