//
//  AppServiceManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 13/02/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
import Firebase
import GooglePlaces
import GoogleMaps
import FirebaseAuth
import CoreLocation
import UserNotifications


class AppServiceManager: NSObject {
    let locationManager = CLLocationManager()
    
  
    lazy var networkReachabilityNotifier:NetworkReachabilityNotifier = NetworkReachabilityNotifier()
    let googleCuAPI_key = "AIzaSyBXmdutJQSR9HkG12YZQ11jzWMpA_JiZrw"
    lazy var appdelegate = UIApplication.shared.delegate as! AppDelegate
    var homeScreenNavController = UINavigationController()
    var mainViewController = UIViewController()
     lazy var appAPNSManager = AppAPNSManager()
    override init() {
        super.init()
        appAPNSManager.delegate = self
        networkReachabilityNotifier.delegate = self
        initiateNetworkReachabilityObserver()
        GMSPlacesClient.provideAPIKey(googleCuAPI_key)
        GMSServices.provideAPIKey(googleCuAPI_key)
        setupAPNS()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        UIApplication.shared.cancelAllLocalNotifications()
    }
    
    
    
    func initiateAppServices() {
       FirebaseApp.configure()
        //Database.database().isPersistenceEnabled = true
        if let jsonString:String  =  UserDefaults.standard.value(forKey: Constant.UserDefaultKeys.userModel) as? String {
            let userModel = User.mapJsonData(jsonString: jsonString)
            if userModel?.emailId == "" {
                navigateToLoginScreen()
            } else {
                AppCacheData.sharedInstance.user = userModel!
                navigateToHomeScreen()
            }
            return
        }
        navigateToLoginScreen()
        
    }
    
    private func navigateToSignUpScreen(){
        let storyBoard:UIStoryboard = UIStoryboard.init(name: Utilities.StoryBoardName.Login.rawValue, bundle: Bundle.main)
        mainViewController = storyBoard.instantiateViewController(withIdentifier:"SignUpViewController") as! SignUpViewController
        (mainViewController as! SignUpViewController).delegate = self
        self.appdelegate.window?.rootViewController = homeScreenNavController
        homeScreenNavController.viewControllers =  [mainViewController]
        homeScreenNavController.isNavigationBarHidden = true
    }
    private func navigateToLoginScreen(){
        let storyBoard:UIStoryboard = UIStoryboard.init(name: Utilities.StoryBoardName.Login.rawValue, bundle: Bundle.main)
        mainViewController = storyBoard.instantiateViewController(withIdentifier:"LoginViewController") as! LoginViewController
        (mainViewController as! LoginViewController).delegate = self
        self.appdelegate.window?.rootViewController = homeScreenNavController
        homeScreenNavController.viewControllers =  [mainViewController]
        homeScreenNavController.isNavigationBarHidden = true
    }
    
    private func navigateToHomeScreen(){
        let storyboard = UIStoryboard(name: Utilities.StoryBoardName.Main.rawValue, bundle: nil)
        mainViewController = storyboard.instantiateViewController(withIdentifier:"HomeViewController") as! HomeViewController
        self.appdelegate.window?.rootViewController = homeScreenNavController
        homeScreenNavController.viewControllers =  [mainViewController]
        homeScreenNavController.isNavigationBarHidden = true
    }


    
}


extension UIApplication {
    
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
}
extension AppServiceManager:SignUpViewControllerDelegate {
    func didPressLoginButton() {
        self.navigateToLoginScreen()
    }
    
}
extension AppServiceManager : LoginViewControllerDelegate {
    func didPressSignUpButton() {
        self.navigateToSignUpScreen()
    }

}

extension AppServiceManager: CLLocationManagerDelegate {
    func handleEvent(forRegion region: CLRegion!) {
        // Show an alert if application is active
        if UIApplication.shared.applicationState == .active {
             let message = "Hey! check the parking slots available near by your \(region.identifier)"
            UIApplication.shared.keyWindow?.window?.rootViewController?.showAlert(withTitle: nil, message: message)
        } else {
            let content = UNMutableNotificationContent()
            content.title = "The Parker"
            content.sound = UNNotificationSound.default
            content.body = "Hey! check the parking slots available near by your \(region.identifier)"
            let badgeNo = UIApplication.shared.applicationIconBadgeNumber + 1
            content.badge = badgeNo as NSNumber?
            let request = UNNotificationRequest(identifier: region.identifier, content: content, trigger: nil)
            UNUserNotificationCenter.current().add(request) { error in
                if (error != nil) {
                }
            }
        }
        
    }

    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region is CLCircularRegion {
            handleEvent(forRegion: region)
        }
    }
}




extension UIViewController {
    func showAlert(withTitle title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
