//
//  AppService+AppLifeCycle.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 28/03/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//
import UIKit
import IQKeyboardManagerSwift

extension(AppServiceManager){
    
    func application(_ application: UIApplication, window:UIWindow?, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        application.statusBarStyle = .default
        initiateAppServices()
        IQKeyboardManager.shared.enable = true
        return true
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {

    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        application.applicationIconBadgeNumber=0
        if(appAPNSManager.apnsManager.apnPermissionStatus == .denied){
            checkAPNPermissionStatus()
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
    }   
    
    
    

}
