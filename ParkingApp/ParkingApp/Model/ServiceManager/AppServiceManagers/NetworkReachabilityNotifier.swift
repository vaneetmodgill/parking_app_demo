//
//  NetworkReachabilityNotifier.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 19/04/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import Foundation
//import ReachabilitySwift

protocol NetworkReachabilityNotifierDelegate:class {
    func networkReachable(isReachable:Bool, rechableType:NetworkReachabilityNotifier.ReachableType)
}

class NetworkReachabilityNotifier:NSObject{
    
    enum ReachableType {
        case Wifi
        case Cellular
        case Unknown
    }
    
    let reachability = Reachability()!
    weak var delegate:NetworkReachabilityNotifierDelegate?
    
    func initateNotification(){
        
        reachability.whenReachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            var currentType:ReachableType = .Unknown
            if reachability.isReachableViaWiFi{
                currentType = .Wifi
            }
            if reachability.isReachableViaWWAN{
                currentType = .Cellular
            }
            
            self.delegate?.networkReachable(isReachable: true, rechableType: currentType)
        }
        reachability.whenUnreachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            self.delegate?.networkReachable(isReachable: false, rechableType: .Unknown)
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        
    }
}

