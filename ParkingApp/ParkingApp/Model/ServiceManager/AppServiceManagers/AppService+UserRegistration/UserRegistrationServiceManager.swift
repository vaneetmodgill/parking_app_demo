//
//  UserRegistrationServiceManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 13/02/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//
import UIKit

protocol UserRegistrationServiceManagerDelegate:class {
    func userRegisteredSuccessfully()
    func userFailedToRegister()
}

class UserRegistrationServiceManager: NSObject, YAServiceManagerDelegate {
    
    weak var delegate:UserRegistrationServiceManagerDelegate?
    //MARK:- PUBLIC METHOD(S)
    func registerUser(delegate:UserRegistrationServiceManagerDelegate){
        self.delegate = delegate
    }
    
    func didCompleteService(sender: YAServiceManager){}
    func failedToCompleteService(sender: YAServiceManager){}
    
}
