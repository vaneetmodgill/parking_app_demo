//
//  AppService+APNS.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 19/02/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

//MARK:- APNS Handler
import UserNotifications

extension(AppServiceManager):APNSManagerDelegate{
    
    func setupAPNS(){
        appAPNSManager.initiateAPNS()
    }
    func checkAPNPermissionStatus(){
        let current = UNUserNotificationCenter.current()
        current.getNotificationSettings(completionHandler: { (settings) in
            
            switch settings.authorizationStatus {
            case .notDetermined:
                self.appAPNSManager.apnsManager.apnPermissionStatus = .notDetermined
                break
            case .denied:
                if self.appAPNSManager.apnsManager.apnPermissionStatus != .denied {
                    AlertCentral.displayAlertForAPNSettings()
                }
                self.appAPNSManager.apnsManager.apnPermissionStatus = .denied
                break
            case .authorized:
                if self.appAPNSManager.apnsManager.apnPermissionStatus == .denied {
                    self.setupAPNS()
                }
                self.appAPNSManager.apnsManager.apnPermissionStatus = .granted
                // It's okay, no need to request
                break
                
            default: break
            }
        })
    }
  
    func setUpDeviceToken(deviceToken:Data){
//        Analytics.sharedInstance.registerTokenForAnalytics(deviceToken: deviceToken)
//        appAPNSManager.setUpDeviceToken(deviceToken: deviceToken)
    }
    
    func setUpReceivedPushMessage(message:[AnyHashable : Any]){
        appAPNSManager.setUpReceivedPushMessage(message: message)
    }
    
    //MARK:- Delegate Method(s)
    func tokenUpdatedSuccessfully(type:AppAPNSManager.TokenType){
        
    }
    func tokenUpdateFailed(type:AppAPNSManager.TokenType){
        YALog.print("###### Token update failed ######")
    }
    
    func didReceivePushMessage(message:PushMessage, isVoip:Bool){
        //If single push message consumed by chat and app then there is an ambigiuity in type
//        startBackgroundHandler()
//        self.connectSocket()
//        self.processPushMessage(message: message)
//        chatManager.processPushMessage(message: message)
//        callManager.processPushMessage(message: message)

    }
    
    func failedToParsePushMessage(error:YAError, isVoip:Bool){
        YALog.print("###### RECEVIED PUSH MESSAGE FAILED \(String(describing: error.debugDescription)) ######")
    }
    
    func didReceiveLocalNotification(userInfo:[AnyHashable:Any]) {
        
//        guard let pushType = userInfo["type"] as? String
//            else {
//
//            return
//        }
//
//        if pushType == LocalNotificationType.CallIncoming.rawValue{
//
//        }
        
      
    }
    
}
