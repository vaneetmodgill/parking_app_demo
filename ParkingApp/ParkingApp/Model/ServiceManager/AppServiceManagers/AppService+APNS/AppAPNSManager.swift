                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        //
//  AppAPNSManager.swift
//  Nehao
//
//  Created by Abhimanu Jindal on 16/02/17.
//  Copyright © 2017 Amit Tripathi. All rights reserved.
//

import UIKit
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        class PushMessage: YADataModel {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            var message:AnyObject?
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            var messageType:String?
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        }

protocol APNSManagerDelegate:class {
    func tokenUpdatedSuccessfully(type:AppAPNSManager.TokenType)
    func tokenUpdateFailed(type:AppAPNSManager.TokenType)
    
    func didReceivePushMessage(message:PushMessage, isVoip:Bool)
    func failedToParsePushMessage(error:YAError, isVoip:Bool)
    
    func didReceiveLocalNotification(userInfo:[AnyHashable:Any])
    
}

class AppAPNSManager: NSObject {
    
    enum TokenType {
        case DeviceToken
        case VoipToken
    }
    
    weak var delegate:APNSManagerDelegate?
    lazy var apnsManager:YAAPNSManager = YAAPNSManager.sharedInstance
//    fileprivate var webServiceManagerUpdateDeviceToken:UpdateDeviceTokenWebServiceManager?
//    fileprivate lazy var webServiceManagerUpdateVoipToken:UpdateVoipTokenWebServiceManager = UpdateVoipTokenWebServiceManager()
    
    
    fileprivate var dbUser:User?
    fileprivate var deviceToken:String?
    fileprivate var voipToken:String?

    override init() {
        super.init()
        apnsManager.delegate = self
     //   webServiceManagerUpdateVoipToken.delegate = self

    }
    
    func initiateAPNS(){
        apnsManager.requestAuthorization { (granted, error) in
            if !granted{
                //FIXME:-HANDLE THE SCENARIO TO SHOW SETTINGS ALERT TO USER IF PUSH NOTIFICATIONS ARE REJECTED BY USER
            }
        }
    }
    
    func setUpDeviceToken(deviceToken:Data){
        apnsManager.setUpDeviceToken(deviceToken: deviceToken)
    }
    
    fileprivate func getCacheUser()->User?{
        return AppCacheData.sharedInstance.user
    }
    
    
}

//MARK:- PUSH MESSAGE EXTENSION
extension(AppAPNSManager){
    
    func setUpReceivedPushMessage(message:[AnyHashable : Any]){
        YALog.print(message)
        setUpMessage(message: message, isVoip:false)
    }
    
    func setUpReceivedVoipMessage(message:[AnyHashable : Any]){
        YALog.print(message)
        setUpMessage(message: message, isVoip:true)
    }
    
    private func setUpMessage(message:[AnyHashable : Any], isVoip:Bool){
        let pushMessage = PushMessage()
        if message["notificationData"] == nil{
            if message["fb_push_card"] == nil {
            let error = YAError.generateError(description: "Message not received in NotificationData", localizedDescription: "Message not received in NotificationData", debugDescription: "Message not received in NotificationData Received Message---> \(message)", code: 404)
            self.delegate?.failedToParsePushMessage(error: error!, isVoip:isVoip)
            return
            } else {
                if UIApplication.shared.applicationState == .active {
                 self.didReceiveLocalNotification(userInfo: message)
                return
                }
            }
        }
        
        let data = message["notificationData"] as? [AnyHashable : Any]
        
        if data == nil{
            let error = YAError.generateError(description: "NotificationData is not in dictionary format", localizedDescription: "NotificationData is not in dictionary format", debugDescription: "NotificationData is not in dictionary format.... Received Message---> \(message)", code: 404)
            self.delegate?.failedToParsePushMessage(error: error!, isVoip:isVoip)
            return
        }
        
        if let notificationMessage = data?[AnyHashable("notificationMessage_1")]{
            pushMessage.message = notificationMessage as AnyObject?
        }
        else{
            let error = YAError.generateError(description: "notificationMessage is not  available", localizedDescription: "notificationMessage is not  available", debugDescription: "notificationMessage is not  available.... Received Message---> \(message)", code: 404)
            self.delegate?.failedToParsePushMessage(error: error!, isVoip:isVoip)
            return
        }
        
        if let notificationType = data?[AnyHashable("type")] as? String{
            pushMessage.messageType = notificationType
        }
        else{
            let error = YAError.generateError(description: "notificationMessageType is not  available", localizedDescription: "notificationMessageType is not  available", debugDescription: "notificationMessageType is not  available.... Received Message---> \(message)", code: 404)
            self.delegate?.failedToParsePushMessage(error: error!, isVoip:isVoip)
            return
        }
        
        self.delegate?.didReceivePushMessage(message: pushMessage, isVoip: isVoip)
    }
    
}

//MARK:- YAAPNSMANAGER DELEGATE METHOD(S)
extension(AppAPNSManager):YAAPNSManagerDelegate{
    
    func didReceiveDeviceToken(token: String) {
//        let user = getCacheUser()
//        if user != nil{
//        }
//        if user?.deviceToken != nil{
//        }
//        if user?.deviceToken == token{
//            self.delegate?.tokenUpdatedSuccessfully(type: .DeviceToken)
//            return
//        }
//        deviceToken = token
//        updateDeviceTokenOnServer(deviceToken: token)
    }
    
    func didReceiveVoipToken(token: String) {
        let user = getCacheUser()
        if user == nil && token == ""{
            return
        }
//        if user?.voipToken != nil{
//        }
//        if user?.voipToken == token{
//            self.delegate?.tokenUpdatedSuccessfully(type: .VoipToken)
//            return
//        }
        voipToken = token
        updateVoipTokenOnServer(voipToken: token)
    }
    
    func didReceiveVoipMessage(message:[AnyHashable : Any]){
        setUpReceivedVoipMessage(message: message)
    }
    
    func didReceiveLocalNotification(userInfo:[AnyHashable:Any]){
     self.delegate?.didReceiveLocalNotification(userInfo: userInfo)
    }
    
}

//MARK:- DEVICE TOKEN EXTENSION
extension(AppAPNSManager): YAServiceManagerDelegate{
    
    //MARK:- DEVICE TOKEN METHOD(S)
    fileprivate func processDeviceTokenWSUpdates(){
        saveDeviceTokenInDB(deviceToken: deviceToken!)
    }
    
    fileprivate func updateDeviceTokenOnServer(deviceToken:String){
//        let user = getCacheUser()
//        if user == nil{
//            //USER NOT YET REGISTERED, AS PER FLOW, APNS REQUEST WILL ONLY BE CALLED ONCE REGISTRATION IS DONE
//        }
//        else{
//            webServiceManagerUpdateDeviceToken = UpdateDeviceTokenWebServiceManager(deviceToken: deviceToken)
//            webServiceManagerUpdateDeviceToken?.call({[weak self] (result) in
//                switch result {
//                case .Success( _):
//                    self?.processDeviceTokenWSUpdates()
//                    break
//                case .Error( _):
//                    YALog.print("####### ERROR IN SAVING USER DEVICE TOKEN ##########")
//                    self?.delegate?.tokenUpdateFailed(type: .DeviceToken)
//                    break
//                }
//            })
//        }
    }
    
    fileprivate func saveDeviceTokenInDB(deviceToken:String){
//        let user = getCacheUser()
//        if user == nil{
//            //USER NOT YET REGISTERED, AS PER FLOW, APNS REQUEST WILL ONLY BE CALLED ONCE REGISTRATION IS DONE
//        }
//        else{
//            user?.deviceToken = deviceToken
//            user?.mapModelToCustomDataModel()
//            saveUserToDB(user: user!, type:.DeviceToken)
//        }
    }
    
    
    //MARK:- VOIP TOKEN METHOD(S)
    fileprivate func processVoipTokenWSUpdates(){
        saveVoipTokenInDB(voipToken: voipToken!)
    }
    
    fileprivate func updateVoipTokenOnServer(voipToken:String){
//        let user = getCacheUser()
//        if user == nil{
//            //USER NOT YET REGISTERED, AS PER FLOW, APNS REQUEST WILL ONLY BE CALLED ONCE REGISTRATION IS DONE
//        }
//        else{
//            webServiceManagerUpdateVoipToken.callWebService(data: voipToken as AnyObject?, accessToken: user?.accessToken, delegate: self)
//        }
    }
    
    fileprivate func saveVoipTokenInDB(voipToken:String){
//        let user = getCacheUser()
//        if user == nil{
//            //USER NOT YET REGISTERED, AS PER FLOW, APNS REQUEST WILL ONLY BE CALLED ONCE REGISTRATION IS DONE
//        }
//        else{
//            user?.voipToken = voipToken
//            DispatchQueue.global(qos: .background).async {
//                user?.mapModelToCustomDataModel()
//                self.saveUserToDB(user: user!, type:.VoipToken)
//            }
//        }
    }
    
    //MARK:- DB METHOD(S)
     
    fileprivate func  saveUserToDB(user:User, type:TokenType){
//        dbServiceManagerUser.callDBService(data: user.customDataModel, methodType: .INSERT, queryString: nil, sortQueryString: nil, sortType: nil) { (resultData, error) in
//            DispatchQueue.global(qos: .background).async {
//                let userModelResult = resultData as? [CustomDataModelUser]
//                let user = User()
//                if (userModelResult?.count == nil ? -1:(userModelResult?.count)!) > 0 {
//                    user.customDataModel = userModelResult?[0]
//                    user.mapCustomDataModelToModel()
//                    DispatchQueue.main.async {
//                        AppCacheData.sharedInstance.user = user
//                        self.delegate?.tokenUpdatedSuccessfully(type: type)
//                    }
//                }
//                else{
//                    DispatchQueue.main.async {
//                        YALog.print("####### ERROR IN SAVING USER DEVICE TOKEN/VOIP TOKEN ##########")
//                        self.delegate?.tokenUpdateFailed(type: type)
//                    }
//                }
//            }
//        }
    }

    
    //MARK:- WEBSERVICE MANAGER DELEGATE METHOD(S)
    func didCompleteService(sender: YAServiceManager) {
//        if sender == webServiceManagerUpdateDeviceToken{
//            processDeviceTokenWSUpdates()
//        }
        
//        if sender == webServiceManagerUpdateVoipToken{
//            processVoipTokenWSUpdates()
//        }
    }
    
    func failedToCompleteService(sender: YAServiceManager) {
//        if sender == webServiceManagerUpdateDeviceToken{
//            YALog.print("####### ERROR IN SAVING USER DEVICE TOKEN ##########")
//            self.delegate?.tokenUpdateFailed(type: .DeviceToken)
//        }
        
//        if sender == webServiceManagerUpdateVoipToken{
//            YALog.print("####### ERROR IN SAVING USER VOIP TOKEN ##########")
//            self.delegate?.tokenUpdateFailed(type: .VoipToken)
//        }
    }
    
}
