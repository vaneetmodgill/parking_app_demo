//
//  AppDelegate.swift
//  WedMe
//
//  Created by Vaneet on 26/03/18.
//  Copyright © 2018 YapApp India Pvt. Ltd. All rights reserved.
//
import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    lazy var applicationServiceManager:AppServiceManager = AppServiceManager()
    
    //MARK:- Application Flow Method(s)
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.isStatusBarHidden = false

        return applicationServiceManager.application(application, window: window, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        applicationServiceManager.applicationWillResignActive(application)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        applicationServiceManager.applicationDidEnterBackground(application)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        applicationServiceManager.applicationWillEnterForeground(application)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        applicationServiceManager.applicationDidBecomeActive(application)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        applicationServiceManager.applicationWillTerminate(application)
    }
    

}

