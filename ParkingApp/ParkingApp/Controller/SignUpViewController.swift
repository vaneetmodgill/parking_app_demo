//
//  SignUpViewController.swift
//  ParkingApp
//
//  Created by Vaneet on 07/08/18.
//  Copyright © 2018 YapApp. All rights reserved.
//

protocol  SignUpViewControllerDelegate:class {
    func didPressLoginButton()
    
}
enum LocationType:String {
    case home
    case work
}

import UIKit
import Firebase
import CoreLocation
class SignUpViewController: YAViewController {
    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var mobileNum: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var homeAddress: UITextField!
    @IBOutlet weak var workAddress: UITextField!
    @IBOutlet weak var carType: UITextField!
    var homeAddressLocation:LocationModel?
    var workAddressLocation:LocationModel?
    var itemPicker = UIPickerView()
    var selectedPickerRow = 0
    var itemToolBar = UIToolbar()
    var carTypes = [NSLocalizedString("HatchBack", comment: ""),NSLocalizedString("Sedan", comment: ""),NSLocalizedString("SUV", comment: "")]
    

    @IBAction func signUpButtonClicked(_ sender: Any) {
        if !Utilities.checkInternetAndShowAlert(){
            return
        }
        if fullName.text == nil || fullName.text?.replace(" ", with: "") == "" {
            displayAlert(title: "", message:  NSLocalizedString("EnterValidName", comment: ""), actionButtonTitle: NSLocalizedString("OK", comment: ""), controller: self)
            return
        }
        if email.text == nil  || !(email.text?.isValidEmail())!{
            displayAlert(title: "", message: NSLocalizedString("EnterValidEmail", comment: ""), actionButtonTitle: NSLocalizedString("OK", comment: ""), controller: self)
            return
        }
        if password.text == nil  || (password.text?.replace(" ", with: "").count)! < 6 {
            displayAlert(title: "", message: NSLocalizedString("EnterValidPassword", comment: ""), actionButtonTitle: NSLocalizedString("OK", comment: ""), controller: self)
            return
        }
        if carType.text == nil  ||  carType.text?.replace(" ", with: "") == "" {
            displayAlert(title: "", message: NSLocalizedString("SelectCarType", comment: "") , actionButtonTitle: NSLocalizedString("OK", comment: ""), controller: self)
            return
        }
        signUpThroughFirebase()
    }
    @IBAction func loginButtonClicked(_ sender: Any) {
        self.delegate?.didPressLoginButton()
    }
    weak var delegate:SignUpViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeItemPicker()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showPicker(_ sender: Any) {
        self.view.addSubview(itemToolBar)
        self.view.addSubview(itemPicker)
    }
    func initializeItemPicker(){
        
        itemPicker.frame = CGRect(x: 0, y: self.view.frame.size.height-220, width: self.view.frame.size.width, height: 220)
        itemPicker.backgroundColor = UIColor.white
        itemPicker.delegate = self
        itemPicker.dataSource = self
        
        itemToolBar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.size.height-264, width:self.view.frame.size.width, height: 44))
        let cancelBtn = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: "") , style: UIBarButtonItem.Style.done, target: self, action: #selector(itemPickerCancelAction))
        
        let doneBtn = UIBarButtonItem(title: NSLocalizedString("Select", comment: "") , style: UIBarButtonItem.Style.done, target: self, action: #selector(itemPickerDoneAction))
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        itemToolBar.setItems([cancelBtn, space, doneBtn], animated: false)
        
    }
    
    
    @objc func itemPickerDoneAction(){
        self.itemPicker.removeFromSuperview()
        self.itemToolBar.removeFromSuperview()
        carType.text = carTypes[selectedPickerRow]
    }
    
    @objc func itemPickerCancelAction(){
        self.itemPicker.removeFromSuperview()
        self.itemToolBar.removeFromSuperview()
    }
    func signUpThroughFirebase() {
        if !Utilities.checkInternetAndShowAlert(){
            return
        }
         CustomLoader.sharedInstance.showActivityIndicator(uiView: self.view, title: "")
        let database = YPDatabaseProvider<CustomDataModelUser>()
        database.request(.getUser(email:email.text!)) { (result) in
            switch result{
            case .success(let dataSnapshot):
                if dataSnapshot.snapshot?.children.allObjects.count != 0 {
                    CustomLoader.sharedInstance.hideActivityIndicator()
                   self.displayAlert(title: "", message:  NSLocalizedString("UserExistDoLogin", comment: ""), actionButtonTitle: NSLocalizedString("OK", comment: ""), controller: self)
                    return
                }
                //signup
                let user = self.getUserModel()
                let database = YPDatabaseProvider<CustomDataModelUser>()
                database.request(.signUpUser(user:user)) { (result) in
                    switch result{
                    case .success( _):
                        CustomLoader.sharedInstance.hideActivityIndicator()
                        AppCacheData.sharedInstance.user = user
                        let jsonString = User.mapModelData(model: user)
                        UserDefaults.standard.set(jsonString, forKey: Constant.UserDefaultKeys.userModel)
                        self.navigateToHomeScreen()
                    case .failure(_):
                         self.showNetworkError()
                    }
                }
            case .failure(_):
                self.showNetworkError()
            }
            
        }
    }
    
    private func getUserModel()->User {
        let user = User()
        user.emailId = self.email.text!
        user.fullName = self.fullName.text!
        user.homeAddress = self.homeAddress.text ?? ""
        user.password = self.password.text ?? ""
        user.workAddress = self.workAddress.text ?? ""
        user.phoneNumber = self.mobileNum.text ?? ""
        user.carType = self.carType.text!
        user.homeAddressLocation = homeAddressLocation
        user.workAddressLocation = workAddressLocation
        user.userId =  Database.database().reference().child("Users").childByAutoId().key!
        return user
    }
    
    private func showNetworkError() {
        CustomLoader.sharedInstance.hideActivityIndicator()
        AlertCentral.displayOkAlert(title: "", message: NSLocalizedString("UnablePerformOperation", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
        }, controller: self)
    }
    
    private func navigateToHomeScreen(){
        let storyboard = UIStoryboard(name: Utilities.StoryBoardName.Main.rawValue, bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier:"HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(mainViewController, animated: true)
    }
    
}
extension SignUpViewController:UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
        
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return carTypes.count
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return carTypes[row]
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedPickerRow = row
        
    }
    
}
extension SignUpViewController:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == homeAddress || textField == workAddress {
            let storyboard = UIStoryboard(name: Utilities.StoryBoardName.Login.rawValue, bundle: nil)
            let mainViewController = storyboard.instantiateViewController(withIdentifier:"SelectLoctionViewController") as! SelectLoctionViewController
            mainViewController.delegate = self
            if textField == homeAddress {
            mainViewController.locationType = .home
            } else  {
                mainViewController.locationType = .work
            }
            self.present(mainViewController, animated: true, completion: {
            })
        }
         
    }
}
extension SignUpViewController:SelectLoctionViewControllerDelegate {
    func selectLoctionViewControllerDidSucesfullySelectedLocation(_ selectLoctionViewController:SelectLoctionViewController, with location:LocationModel,locationType:LocationType) {
        switch locationType {
        case .work:
            workAddressLocation = location
            workAddress.text = location.address
            workAddress.resignFirstResponder()
       case .home:
        homeAddressLocation = location
        homeAddress.text = location.address
        homeAddress.resignFirstResponder()
        }
         selectLoctionViewController.dismiss(animated: true, completion: nil)
    }
    func selectLoctionViewControllerDismisButtonPressed(_ selectLoctionViewController:SelectLoctionViewController) {
        selectLoctionViewController.dismiss(animated: true, completion: nil)

    }
}

