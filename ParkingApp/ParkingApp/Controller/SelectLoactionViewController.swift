//
//  SelectLoactionViewController.swift
//  RaysApp
//
//  Created by Vaneet on 17/08/18.
//  Copyright © 2018 YapApp. All rights reserved.
//

import UIKit
import GooglePlacePicker
import GooglePlaces
import GoogleMaps
import CoreLocation

protocol SelectLoctionViewControllerDelegate {
    func selectLoctionViewControllerDidSucesfullySelectedLocation(_ selectLoctionViewController:SelectLoctionViewController, with location:LocationModel, locationType:LocationType)
    func selectLoctionViewControllerDismisButtonPressed(_ selectLoctionViewController:SelectLoctionViewController)
}

class SelectLoctionViewController: YAViewController {
    //MARK: IBOUTLET(S)
    @IBOutlet private weak var mapView: GMSMapView!
    @IBOutlet private weak var screenTitle: UINavigationItem!
    @IBOutlet private weak var locationTextField: UITextField!
    @IBOutlet private weak var detectCurrenLocButton: UIButton!
    @IBOutlet private weak var setLocationButton: UIView!
    @IBOutlet private weak var setLocationLabel: UILabel!
    
    //MARK: VARIABLE(S)
    private var locManager = CLLocationManager()
    private var locationSetManually = false
    private var userLocation = LocationModel()
    var delegate:SelectLoctionViewControllerDelegate?
    var locationType:LocationType = .work
    
    //MARK: IBACTION(S)
    @IBAction private func backButtonPressed(_ sender: Any) {
        self.delegate?.selectLoctionViewControllerDismisButtonPressed(self)
    }
    @IBAction private func setLocationButtonPressed(_ sender: Any) {
       
        self.delegate?.selectLoctionViewControllerDidSucesfullySelectedLocation(self, with: self.userLocation, locationType: locationType)
    }
    @IBAction private func detectCurrentLocationButtonPressed(_ sender: Any) {
        if self.mapView.myLocation == nil {
            AlertCentral.displayOkAlert(title: "", message:NSLocalizedString("AllowLocationFromSettings", comment: "") , style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: "") , completionHandler: { (action) in
            }, controller: self)
         return
        }
        guard let lat = self.mapView.myLocation?.coordinate.latitude,
        let lng = self.mapView.myLocation?.coordinate.longitude else { return }
        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 20.0)
        self.mapView.animate(to: camera)
    }
    
    
    //MARK: LIFECYCLE METHOD(S)
    override func viewDidLoad() {
        super.viewDidLoad()
        locationTextField.delegate = self
        setupUserExperience()
        getUserLocationAuth()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: PRIVATE METHOD(S)
    private func setupUserExperience(){
        screenTitle.title =  NSLocalizedString("selectLocation", comment: "")
        locationTextField.placeholder =  NSLocalizedString("selectLocation", comment: "")
        detectCurrenLocButton.setTitle(  NSLocalizedString("detectLocations", comment: ""), for:.normal)
        setLocationLabel.text = NSLocalizedString("setLocation", comment: "")
    }

    
    private func getUserLocationAuth(){
        CustomLoader.sharedInstance.showActivityIndicator(uiView: self.view, title: NSLocalizedString("GettingLocation", comment: ""))
        locManager.requestWhenInUseAuthorization()
        locManager.delegate = self
        self.mapView.isMyLocationEnabled = true
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
//            DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
//                self.currentLocation = self.locManager.location
//                self.locManager.startUpdatingLocation()
//            })
        }
        else if  CLLocationManager.authorizationStatus() == .restricted ||
            CLLocationManager.authorizationStatus() ==  .denied{
            CustomLoader.sharedInstance.hideActivityIndicator()
            AlertCentral.displayOkAlert(title: "", message: NSLocalizedString("AllowLocationFromSettings", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: "") , completionHandler: { (action) in
            }, controller: self)
        }
    }
    
    private func showMapView(){
        let camera = GMSCameraPosition.camera(withLatitude: userLocation.locationLatitude, longitude: userLocation.locationLongitude, zoom: 20.0)
        mapView.camera = camera
        mapView.isMyLocationEnabled = true
        mapView.settings.compassButton = true
        mapView.delegate = self
    }
    
    
    private func saveLatLong(){
        
    }
    
    
    
}


extension SelectLoctionViewController:GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        setLocationButton.isHidden = true
    }
  
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        setLocationButton.isHidden = false
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if locationSetManually {
            locationSetManually = false
            return
        }
        let geocoder = GMSGeocoder()
        var currentAddress = String()
        geocoder.reverseGeocodeCoordinate(position.target) { (response, error) in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                currentAddress = lines.joined(separator:", ")
                self.userLocation.address = currentAddress
                self.userLocation.city = address.locality ?? ""
                self.userLocation.pincode = address.postalCode ?? ""
                self.userLocation.state = address.administrativeArea ?? ""
                self.userLocation.country = address.country ?? ""
                
            }
            if currentAddress != "" {
            self.locationTextField.text = currentAddress
               
            } else {
                AlertCentral.displayOkAlert(title: "", message: NSLocalizedString("UnableToGetCurrentLocation", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
                    let autocompleteController = GMSAutocompleteViewController()
                    autocompleteController.delegate = self
                    self.present(autocompleteController, animated: true, completion: nil)
                }, controller: self)
            }
    }
}

}




//MARK:- Location manager Delegate
extension SelectLoctionViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         CustomLoader.sharedInstance.hideActivityIndicator()
        userLocation.locationLatitude = locations[0].coordinate.latitude
        userLocation.locationLongitude = locations[0].coordinate.longitude
        showMapView()
        self.locManager.stopUpdatingLocation()
       
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse{
            self.locManager.startUpdatingLocation()
        } else if status == .denied || status == .restricted {
            CustomLoader.sharedInstance.hideActivityIndicator()
        }
    }
    
}


extension SelectLoctionViewController:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
}

extension SelectLoctionViewController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        locationSetManually = true
        userLocation.locationLatitude = place.coordinate.latitude
        userLocation.locationLongitude = place.coordinate.longitude
        showMapView()
        self.locationTextField.text = place.formattedAddress
        dismiss(animated: true, completion: nil)
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(place.coordinate) { (response, error) in
            if let address = response?.firstResult() {
                self.userLocation.address = place.formattedAddress ?? ""
                self.userLocation.city = address.locality ?? ""
                self.userLocation.pincode = address.postalCode ?? ""
                self.userLocation.state = address.administrativeArea ?? ""
                self.userLocation.country = address.country ?? ""
                
            }
        }

        
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

