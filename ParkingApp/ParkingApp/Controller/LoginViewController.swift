//
//  LoginViewController.swift
//  ParkingApp
//
//  Created by Vaneet on 07/08/18.
//  Copyright © 2018 YapApp. All rights reserved.
//

import UIKit
import Firebase
protocol LoginViewControllerDelegate:class {
    func didPressSignUpButton()
}

class LoginViewController: YAViewController {
    
    //MARK: IBOUTLET(S)
    @IBOutlet private var txtPassword: UITextField!
    @IBOutlet private var txtUsername: UITextField!
    
    //MARK: VARIABLE(S)
    weak var delegate:LoginViewControllerDelegate?
    
    //MARK: VIEW LIFECYCLE METHOD(S)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUserExperience()
    }
    
    
    
    //MARK: MEMORY MANAGEMENT
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        self.delegate?.didPressSignUpButton()
    }
    
    //MARK: IBACTION(S)
    
    @IBAction private func btnLogin(_ sender: UIButton) {
        if  !isUserNameValid() || !isPasswordValid(){
            return
        }
        if !Utilities.checkInternetAndShowAlert(){
            return
        }
        CustomLoader.sharedInstance.showActivityIndicator(uiView: self.view, title: "")
        let database = YPDatabaseProvider<CustomDataModelUser>()
        database.request(.getUser(email:txtUsername.text!)) { (result) in
            switch result{
            case .success(let dataSnapshot):
                CustomLoader.sharedInstance.hideActivityIndicator()
                if dataSnapshot.snapshot?.children.allObjects.count == 0 {
                    self.displayAlert(title: "", message: NSLocalizedString("UserNotExist", comment: ""), actionButtonTitle: NSLocalizedString("OK", comment: ""), controller: self)
                    return
                }
                for child in dataSnapshot.snapshot?.children.allObjects as? [DataSnapshot] ?? [] {
                    if let model = child.value as? [String : AnyObject]{
                        if let userModel = User.mapJsonDictionaryData(jsonDictionary: model){
                            if self.txtPassword.text != userModel.password {
                                self.displayAlert(title: "", message: NSLocalizedString("PasswordNotMached", comment: ""), actionButtonTitle: NSLocalizedString("OK", comment: ""), controller: self)
                                return
                            }
                            AppCacheData.sharedInstance.user = userModel
                            let jsonString = User.mapModelData(model: userModel)
                            UserDefaults.standard.set(jsonString, forKey: Constant.UserDefaultKeys.userModel)
                            self.navigateToHomeScreen()
                            return
                        }
                    }
                }
                
            case .failure(_):
                self.showNetworkError()
                
            }
            
        }
    }
    private func showNetworkError() {
        CustomLoader.sharedInstance.hideActivityIndicator()
        AlertCentral.displayOkAlert(title: "", message: NSLocalizedString("UnablePerformOperation", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
        }, controller: self)
    }
    private func navigateToHomeScreen(){
        let storyboard = UIStoryboard(name: Utilities.StoryBoardName.Main.rawValue, bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier:"HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(mainViewController, animated: true)
    }
    //MARK:- PRIVATE METHOD(S)
    private func setupUserExperience() {
        UIApplication.shared.isStatusBarHidden = true
    }
    
    private func isUserNameValid()->Bool {
        if txtUsername.text == nil  || !(txtUsername.text?.isValidEmail())!{
            displayAlert(title: "", message: NSLocalizedString("EnterValidEmail", comment: ""), actionButtonTitle: NSLocalizedString("OK", comment: ""), controller: self)
            return false
        }
        return true
    }
    
    private func isPasswordValid()->Bool {
        
        if txtPassword.text == nil  || (txtPassword.text?.replace(" ", with: "").count)! < 6 {
            displayAlert(title: "", message: NSLocalizedString("EnterValidPassword", comment: ""), actionButtonTitle: NSLocalizedString("OK", comment: ""), controller: self)
            return false
        }
        return true
    }
    
    
}


