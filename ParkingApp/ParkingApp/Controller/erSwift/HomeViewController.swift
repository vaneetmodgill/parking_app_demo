//
//  HomeViewController.swift
//  ParkingApp
//
//  Created by Tina Gupta on 16/07/18.
//  Copyright © 2018 YapApp. All rights reserved.
//

import UIKit
import GooglePlacePicker
import GooglePlaces
import GoogleMaps
//import GeoFire
import FirebaseDatabase
import CoreLocation
import UserNotifications


let isUserParked = "isUserParked"
let userLat = "userLat"
let userLong = "userLong"
let isMonitoring = "isMonitoring"

class HomeViewController: UIViewController {
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var parkedView: UIView!
    @IBOutlet weak var notParkedView: UIView!
    
    
    var userCurrentCity:String?
    var currentLocation: CLLocation!
    var isLookingForParking = false
    var geoLocationModelArray = [GeolocationModel]()
    var locManager = CLLocationManager()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getUserLocationAuth()
        self.updateViewForUser()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpGeofenceForworkAndHome()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:--
    func updateViewForUser(){
        if UserDefaults.standard.bool(forKey: isUserParked) == true{
            geoLocationModelArray.removeAll()
            mapView.clear()
            if let latitude = UserDefaults.standard.object(forKey: userLat), let longitude = UserDefaults.standard.object(forKey: userLong){
                self.mapView.isMyLocationEnabled = false
                self.mapView.settings.compassButton = false
                self.mapView.settings.myLocationButton = false
                let marker = GMSMarker()
                marker.appearAnimation = .pop
                marker.icon = UIImage(named: "car")
                marker.position = CLLocationCoordinate2D(latitude: latitude as! CLLocationDegrees, longitude: longitude as! CLLocationDegrees)
                marker.title = "Your Car"
                marker.map = mapView
            }
            self.parkedView.isHidden = false
            self.notParkedView.isHidden = true
        }else{
            mapView.clear()
            
            self.parkedView.isHidden = true
            self.notParkedView.isHidden = false
            
        }
        
    }
    
    func updateUserDefaults(isParked:Bool, for location:CLLocation?){
        UserDefaults.standard.set(isParked, forKey: isUserParked)
        if location != nil{
            UserDefaults.standard.set(location?.coordinate.latitude as? Double, forKey: userLat)
            UserDefaults.standard.set(location?.coordinate.longitude as? Double, forKey: userLong)
            
        }else{
            UserDefaults.standard.removeObject(forKey: userLat)
            UserDefaults.standard.removeObject(forKey: userLong)
            
        }
        UserDefaults.standard.synchronize()
        self.updateViewForUser()
    }

    
    @IBAction func parkButtonPressed(_ sender: Any) {
        if self.userCurrentCity == nil{
            AlertCentral.displayOkAlert(title: "The Parker", message: NSLocalizedString("UnableToGetCurrentLocation", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
                self.getUserLocationAuth()
            }, controller: self)
        }else{
            //            if currentLocation.horizontalAccuracy > 100{
            //                self.isLookingForParking = true
            //                CustomLoader.sharedInstance.showActivityIndicator(uiView: self.view, title: "Trying to get accurate location")
            //            }else{
            CustomLoader.sharedInstance.hideActivityIndicator()
            self.isLookingForParking = false
            self.checkForGeoLocation(location: currentLocation, for: self.userCurrentCity!)
            // }
        }
        
    }
    
    @IBAction func locateCarAction(_ sender: Any) {
        CustomLoader.sharedInstance.showActivityIndicator(uiView: self.view, title: NSLocalizedString("LocatingCar", comment: ""))
        
        if self.userCurrentCity == nil{
            CustomLoader.sharedInstance.hideActivityIndicator()
            
            AlertCentral.displayOkAlert(title: " The Parker", message: NSLocalizedString("UnableToGetCurrentLocation", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
                self.getUserLocationAuth()
            }, controller: self)
        }else{
            let database = YPDatabaseProvider<CustomDataModelGeolocation>()
            let userId = AppCacheData.sharedInstance.getUserId()
            database.request(.getUserParkingData(userId: userId, forKey: self.userCurrentCity!)) { (result) in
                switch result{
                case .success(let dataSnapshot):
                    CustomLoader.sharedInstance.hideActivityIndicator()
                    
                    for child in dataSnapshot.snapshot?.children.allObjects as? [DataSnapshot] ?? [] {
                        if let model = child.value as? [String : AnyObject]{
                            if let locModel = GeolocationModel.mapJsonDictionaryData(jsonDictionary: model){
                                if locModel.userId == userId{
                                    self.openGoogleMapForPlace(lat: locModel.latitude!, long: locModel.longitude!, mode: "walking")
                                }
                            }
                        }
                    }
                    CustomLoader.sharedInstance.hideActivityIndicator()
                    
                case .failure(_):
                    CustomLoader.sharedInstance.hideActivityIndicator()
                    AlertCentral.displayOkAlert(title: " The Parker", message: NSLocalizedString("UnableToLocateCar", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
                        self.getUserLocationAuth()
                    }, controller: self)
                    print("failed")
                    
                }
                
            }
            
        }
    }
    
    @IBAction func freeParkingButtonAction(_ sender: Any) {
        if !Utilities.checkInternetAndShowAlert(){
            //Internet Alert
            return
        }
        CustomLoader.sharedInstance.showActivityIndicator(uiView: self.view, title: "")
        if self.userCurrentCity == nil{
            CustomLoader.sharedInstance.hideActivityIndicator()
            AlertCentral.displayOkAlert(title: " The Parker", message: NSLocalizedString("UnableToGetCurrentLocation", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
                self.getUserLocationAuth()
            }, controller: self)
        }else{
            let database = YPDatabaseProvider<CustomDataModelGeolocation>()
            let userId = AppCacheData.sharedInstance.getUserId()
            database.request(.getUserParkingData(userId: userId, forKey: self.userCurrentCity!)) { (result) in
                switch result{
                case .success(let dataSnapshot):
                    for child in dataSnapshot.snapshot?.children.allObjects as? [DataSnapshot] ?? [] {
                        if let model = child.value as? [String : AnyObject]{
                            if let locModel = GeolocationModel.mapJsonDictionaryData(jsonDictionary: model){
                                if locModel.userId == userId{
                                    locModel.isAvailable = true
                                    locModel.userId = ""
                                    let nodeKey = self.userCurrentCity! + "/" + child.ref.key!
                                    database.request(.updateAvailabilityData(lcoationModel: locModel, forKey: nodeKey))
                                    self.updateUserDefaults(isParked: false, for: nil)
                                    CustomLoader.sharedInstance.hideActivityIndicator()
                                    AlertCentral.displayOkAlert(title: " The Parker", message: NSLocalizedString("SlotMarkedFree", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
                                        self.getUserLocationAuth()
                                    }, controller: self)
                                }
                            }
                        }
                    }
                    CustomLoader.sharedInstance.hideActivityIndicator()
                    
                case .failure(_):
                    CustomLoader.sharedInstance.hideActivityIndicator()
                    AlertCentral.displayOkAlert(title: " The Parker", message: NSLocalizedString("UnablePerformOperation", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
                        self.getUserLocationAuth()
                    }, controller: self)
                    print("failed")
                    
                }
                
            }
        }
    }
    
    
    //MARK:- Get User Location Authentication
    func getUserLocationAuth(){
        CustomLoader.sharedInstance.showActivityIndicator(uiView: self.view, title: NSLocalizedString("GettingLocation", comment: ""))
        locManager.requestAlwaysAuthorization()
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if(CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute: {
                self.currentLocation = self.locManager.location
                self.locManager.startUpdatingLocation()
                self.getUserCurrentCity()
                self.showMapView()
            })
        }
        else if  CLLocationManager.authorizationStatus() == .restricted ||
            CLLocationManager.authorizationStatus() ==  .denied{
            CustomLoader.sharedInstance.hideActivityIndicator()
            AlertCentral.displayOkAlert(title: " The Parker", message: NSLocalizedString("AllowLocationFromSettings", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
            }, controller: self)
        }
      

    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse{
            self.currentLocation = self.locManager.location
            self.locManager.startUpdatingLocation()
            self.getUserCurrentCity()
        } else if status == .denied || status == .restricted {
            CustomLoader.sharedInstance.hideActivityIndicator()
            AlertCentral.displayOkAlert(title: " The Parker", message: NSLocalizedString("AllowLocationFromSettings", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
            }, controller: self)
        }
}
    
    func saveParkingData(for key:String, with status:Bool, baseKey: String, locationModel: GeolocationModel){
        
        let database = YPDatabaseProvider<CustomDataModelGeolocation>()
        if !Utilities.checkInternetAndShowAlert(){
            //Internet Alert
            return
        }
        CustomLoader.sharedInstance.showActivityIndicator(uiView: self.view, title: NSLocalizedString("MarkingParkingSlot", comment: ""))

        let city = baseKey + "/" + key
        let userId = AppCacheData.sharedInstance.getUserId()
        locationModel.userId = userId
        locationModel.isAvailable = false
        database.request(.updateAvailabilityData(lcoationModel: locationModel, forKey: city)) { (result) in
            switch result{
            case .success(_):
                CustomLoader.sharedInstance.hideActivityIndicator()
                
                AlertCentral.displayOkAlert(title: " The Parker", message: NSLocalizedString("SlotBookedSuccessfully", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
                    self.updateUserDefaults(isParked: true, for: CLLocation(latitude: locationModel.latitude!, longitude: locationModel.longitude!))
                }, controller: self)
                
            case .failure(_):
                CustomLoader.sharedInstance.hideActivityIndicator()
                AlertCentral.displayOkAlert(title: " The Parker", message: NSLocalizedString("UnablePerformOperation", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
                },  controller: self)
                print("failed")
                
            }
        }
        
    }
    
}

//MARK:- Location manager Delegate
extension HomeViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations[0]
        if isLookingForParking{
            self.parkButtonPressed(UIButton())
        }
    }
    
}
//MARK:- GeoFire Location Data Methods

extension HomeViewController{
    
    func saveGeoData(cityName:String,location:CLLocation){
        
        let firebase = Database.database().reference().child("Locations").child(cityName)
        
        let database = YPDatabaseProvider<CustomDataModelGeolocation>()
        if !Utilities.checkInternetAndShowAlert(){
            //Internet Alert
            return
        }
        CustomLoader.sharedInstance.showActivityIndicator(uiView: self.view, title: NSLocalizedString("MarkingParkingSlot", comment: ""))

        let city = cityName + "/" + firebase.childByAutoId().key!
        
        let locationModel = GeolocationModel()
        locationModel.latitude = location.coordinate.latitude
        locationModel.longitude = location.coordinate.longitude
        locationModel.isAvailable = false
        database.request(.addLocationData(locationModel: locationModel, forKey: city)) { (result) in
            switch result{
            case .success(_):
                CustomLoader.sharedInstance.hideActivityIndicator()
                
                AlertCentral.displayOkAlert(title: " The Parker", message: NSLocalizedString("SlotBookedSuccessfully", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
                    self.updateUserDefaults(isParked: true, for: CLLocation(latitude: locationModel.latitude!, longitude: locationModel.longitude!))
                }, controller: self)
                
            case .failure(_):
                CustomLoader.sharedInstance.hideActivityIndicator()
                AlertCentral.displayOkAlert(title: " The Parker", message: "Unable to mark your slot. Try Again.", style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
                },  controller: self)
                print("failed")
                
            }
        }
    }
    
    func checkForGeoLocation(location:CLLocation?, for cityName:String){
        
        CustomLoader.sharedInstance.showActivityIndicator(uiView: self.view, title: NSLocalizedString("CheckingCurrentLocation", comment: ""))
        
        let database = YPDatabaseProvider<CustomDataModelGeolocation>()
        database.request(.getLocationData(forKey: cityName)){ (result) in
            switch result{
            case .success(let dataSnapshot):
                CustomLoader.sharedInstance.hideActivityIndicator()
                
                let snapShot = dataSnapshot.snapshot
                var locationsArray = [GeolocationModel]()
                if snapShot?.children.allObjects.count == 0{
                    self.saveGeoData(cityName: self.userCurrentCity!, location: location!)
                    return
                }
                for child in snapShot?.children.allObjects as? [DataSnapshot] ?? [] {
                    var loc = GeolocationModel()
                    if let model = child.value as? [String : AnyObject]{
                        if let locModel = GeolocationModel.mapJsonDictionaryData(jsonDictionary: model){
                            loc = locModel
                            locationsArray.append(loc)
                            //Check Distance
                            
                            if Int((locModel.location?.distance(from: location!))!) <= 5 {
                                if locModel.isAvailable == true {
                                    //Show want to park alert
                                    self.showParkingAlert(for: child.ref.key!, baseKey: (snapShot?.ref.key)!, locationModel: locModel)
                                }else{
                                    AlertCentral.displayOkAlert(title: " The Parker", message:  NSLocalizedString("slotAlreadyBooked", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
                                    }, controller: self)
                                }
                                return
                            }
                        }
                        
                    }
                    
                }
                self.saveGeoData(cityName: self.userCurrentCity!, location: location!)
                
                print(dataSnapshot)
                
            case .failure(_):
                CustomLoader.sharedInstance.hideActivityIndicator()
                print("failed")
                
            }
        }
        
    }
    
    
    func queryForParkingStatus(at locationKey:String){
        let database = YPDatabaseProvider<CustomDataModelParkingStatus>()
        if !Utilities.checkInternetAndShowAlert(){
            //Internet Alert
            return
        }
        database.request(.getParkingData(forKey: locationKey)) { (result) in
            switch result{
            case .success(let parkingSnapshot):
                print(parkingSnapshot)
                let snapshot = parkingSnapshot.snapshot
                if let model = snapshot?.value as? [String : AnyObject]{
                    if let parkingModel = ParkingStatusModel.mapJsonDictionaryData(jsonDictionary: model){
                        if parkingModel.isAvailable == false{
                            //Show Alreadyy booked alert
                            
                            AlertCentral.displayOkAlert(title: " The Parker", message: NSLocalizedString("slotAlreadyBooked", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
                            }, controller: self)
                        }else{
                            //Show want to park alert
                            // self.showParkingAlert(for: (snapshot?.key)!, b)
                        }
                    }
                }
            case .failure(_):
                print("failed")
                
            }
        }
    }
    
    func showParkingAlert(for key:String, baseKey: String, locationModel: GeolocationModel){
        let alertController = UIAlertController(title: " The Parker", message: NSLocalizedString("SlotAvailable", comment: ""), preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default) { (_) in
            self.saveParkingData(for: key, with: false, baseKey:baseKey, locationModel: locationModel )
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString(NSLocalizedString("No", comment: ""), comment: ""), style: .cancel) { (_) in
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}


//MARK:- Getting Geolocation Data

extension HomeViewController{
    func getLocationCity(for location: CLLocation, completion:@escaping ((_ locality: String?, _ error: Error?) -> Void)){
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(location.coordinate) { (response, error) in
            if response?.results() == nil{
                completion(nil, error)
                return
            }
            for adress in (response?.results())!{
                if adress.locality != nil{
                    completion(adress.locality, nil)
                    return
                }
            }
        }
    }
    
    
    func getUserCurrentCity(){
        if currentLocation != nil{
            self.getLocationCity(for: currentLocation!) { (location, error) in
                if error == nil && location != nil{
                    CustomLoader.sharedInstance.hideActivityIndicator()
                    self.userCurrentCity = location
                    if !Utilities.checkInternetAndShowAlert(){
                        return
                    }
                    if self.userCurrentCity == nil || self.currentLocation == nil{
                        return
                    }
                    if UserDefaults.standard.bool(forKey: isUserParked) == false{
                        self.searchForLocation(location: self.currentLocation, for: self.userCurrentCity!)
                        
                    } else {
                        self.mapView.isMyLocationEnabled = false
                        self.mapView.settings.compassButton = false
                        self.mapView.settings.myLocationButton = false
                    }
                    
                }else{
                    
                    CustomLoader.sharedInstance.hideActivityIndicator()
                    
                    AlertCentral.displayOkAlert(title: "Parking", message: NSLocalizedString("UnableToGetCurrentLocation", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
                        
                    }, controller: self)
                }
            }
        }else{
            CustomLoader.sharedInstance.hideActivityIndicator()
            if !Utilities.checkInternetAndShowAlert(){
                return
            }
            AlertCentral.displayOkAlert(title: "Parking", message: NSLocalizedString("UnableToGetCurrentLocation", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
                
            }, controller: self)
        }
    }
}

extension HomeViewController {
    func showMapView(){
        if currentLocation == nil {
            return
        }
        let camera = GMSCameraPosition.camera(withLatitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude, zoom: 20.0)
        mapView.camera = camera
        mapView.isMyLocationEnabled = true
        // GOOGLE MAPS SDK: COMPASS
        mapView.settings.compassButton = true
        mapView.delegate = self
       
        // GOOGLE MAPS SDK: USER'S LOCATION
        mapView.settings.myLocationButton = true
        for (i,singleCordinate) in self.geoLocationModelArray.enumerated() {
            let marker = GMSMarker()
            marker.appearAnimation = .pop
            if singleCordinate.isAvailable {
            marker.icon = UIImage(named: "parking")
            } else {
                marker.icon = UIImage(named: "no-parking")
            }
            marker.userData = singleCordinate.isAvailable
            marker.position = CLLocationCoordinate2D(latitude: singleCordinate.latitude!, longitude: singleCordinate.longitude!)
            marker.title = "\(i) Parking"
            marker.map = mapView
        }
    }
    
    func searchForLocation(location:CLLocation?, for cityName:String){
        let database = YPDatabaseProvider<CustomDataModelGeolocation>()
        database.request(.getLocationData(forKey: cityName)){ (result) in
            switch result{
            case .success(let dataSnapshot):
                let snapShot = dataSnapshot.snapshot
                var locationsArray = [GeolocationModel]()
                if snapShot?.children.allObjects.count == 0{
                    AlertCentral.displayOkAlert(title: "Parking", message:NSLocalizedString("NoSlotAvailable", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in  }, controller: self)
                    return
                }
                for child in snapShot?.children.allObjects as? [DataSnapshot] ?? [] {
                    var loc = GeolocationModel()
                    if let model = child.value as? [String : AnyObject]{
                        if let locModel = GeolocationModel.mapJsonDictionaryData(jsonDictionary: model){
                            loc = locModel
                            locationsArray.append(loc)
                            //Check Distance
                            
                            if Int((locModel.location?.distance(from: location!))!) <= 2000 {
//                                if locModel.isAvailable == true {
                                    self.geoLocationModelArray.append( locModel)
 //                               }
                            }
                        }
                        
                    }
                    
                }
                
                if self.geoLocationModelArray.count == 0 {
                    AlertCentral.displayOkAlert(title: " The Parker", message: NSLocalizedString("NoSlotAvailable", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in  }, controller: self)
                    return
                }
                self.showMapView()
                
                
            case .failure(_):
                print("failed")
                
            }
        }
        
    }
}

extension HomeViewController:GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        if let userData  = marker.userData as? Bool {
            if userData{
                openGoogleMapForPlace(lat: marker.position.latitude, long: marker.position.longitude, mode: "driving")
               return
            }
            AlertCentral.displayOkAlert(title: " The Parker", message: NSLocalizedString("slotAlreadyBooked", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
            }, controller: self)
            return
        }
        openGoogleMapForPlace(lat: marker.position.latitude, long: marker.position.longitude, mode: "driving")
    }
    
    /* set a custom Info Window */
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        let view = MakerInfoView(frame: CGRect.init(x: 0, y: 0, width: 170, height: 50))
        if let userData  = marker.userData as? Bool {
            if userData{
            view.navigateLocLabel.text = NSLocalizedString("NavigateToLocation", comment: "")
            } else {
                view.navigateLocLabel.text = NSLocalizedString("slotNotFree", comment: "")
            }
            
        } else {
        view.navigateLocLabel.text = NSLocalizedString("NavigateToLocation", comment: "")
        }
        
//        let view = UIView(frame: CGRect.init(x: 0, y: 0, width: 200, height: 70))
//        view.backgroundColor = UIColor.white
//        view.layer.cornerRadius = 6
//        
//        let lbl1 = UILabel(frame: CGRect.init(x: 10, y: 16, width: view.frame.size.width - 14, height: 20))
//        lbl1.text = NSLocalizedString("NavigateToLocation", comment: "")
//        lbl1.textColor = UtiltityMethods.colorWithHexString(hex: "3AB54A")
//        view.addSubview(lbl1)
        return view
    }
    private func openGoogleMapForPlace (lat:Double,long:Double, mode:String){
        if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps:")!) {
            let urlString = "comgooglemaps://?daddr=\(lat),\(long)&directionsmode=\(mode)"
            UIApplication.shared.openURL(URL(string: urlString)!)
        }
        else {
            let string = "http://maps.google.com/maps?ll=\(lat),\(long)&directionsmode=driving"
            UIApplication.shared.openURL(URL(string: string)!)
        }
    }
}



extension HomeViewController {
    func setUpGeofenceForworkAndHome() {
        if UserDefaults.standard.bool(forKey: isMonitoring) == true{
          return
            
        }
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            showAlert(withTitle:"Error", message: "Geofencing is not supported on this device!")
            return
        }
        // 2
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            showAlert(withTitle:"Warning", message: "Your geotification is saved but will only be activated once you grant Geotify permission to access the device location.")
        }
        
        
        if AppCacheData.sharedInstance.user.workAddressLocation != nil {
            let geofenceRegionCenter = CLLocationCoordinate2DMake(AppCacheData.sharedInstance.user.workAddressLocation?.locationLatitude ?? 0.0, AppCacheData.sharedInstance.user.workAddressLocation?.locationLongitude ?? 0.0);
            let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: 500, identifier: LocationType.work.rawValue);
            //geofenceRegion.notifyOnExit = true;
            geofenceRegion.notifyOnEntry = true;
            self.locManager.startMonitoring(for: geofenceRegion)
            UserDefaults.standard.set(true, forKey: isMonitoring)
        }
        if AppCacheData.sharedInstance.user.homeAddressLocation != nil {
            let geofenceRegionCenter = CLLocationCoordinate2DMake(AppCacheData.sharedInstance.user.homeAddressLocation?.locationLatitude ?? 0.0, AppCacheData.sharedInstance.user.homeAddressLocation?.locationLongitude ?? 0.0);
            let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: 500, identifier: LocationType.home.rawValue);
            //geofenceRegion.notifyOnExit = true;
            geofenceRegion.notifyOnEntry = true;
            print(self.locManager.requestState(for: geofenceRegion))
            self.locManager.startMonitoring(for: geofenceRegion)
            UserDefaults.standard.set(true, forKey: isMonitoring)
        }
    }
}
