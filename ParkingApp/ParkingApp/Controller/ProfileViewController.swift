//
//  ProfileViewController.swift
//  ParkingApp
//
//  Created by Vaneet on 04/09/18.
//  Copyright © 2018 YapApp. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation
class ProfileViewController: YAViewController {
    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var mobileNum: UITextField!
    @IBOutlet weak var homeAddress: UITextField!
    @IBOutlet weak var workAddress: UITextField!
    @IBOutlet weak var carType: UITextField!
    var homeAddressLocation:LocationModel?
    var workAddressLocation:LocationModel?
    var itemPicker = UIPickerView()
    var selectedPickerRow = 0
    var itemToolBar = UIToolbar()
    var carTypes = [NSLocalizedString("HatchBack", comment: ""),NSLocalizedString("Sedan", comment: ""),NSLocalizedString("SUV", comment: "")]
    
    @IBAction func backButtonPresses(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func logoutButtonPressed(_ sender: Any) {
        UserDefaults.standard.set(nil, forKey: Constant.UserDefaultKeys.userModel)
        UserDefaults.standard.set(false, forKey: isMonitoring)
        UserDefaults.standard.set(false, forKey: isUserParked)
        let storyBoard:UIStoryboard = UIStoryboard.init(name: Utilities.StoryBoardName.Login.rawValue, bundle: Bundle.main)
        let mainViewController = storyBoard.instantiateViewController(withIdentifier:"LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(mainViewController, animated: true)
        
    }
    @IBAction func submitButtonClicked(_ sender: Any) {
        if !Utilities.checkInternetAndShowAlert(){
            return
        }
        if fullName.text == nil || fullName.text?.replace(" ", with: "") == "" {
            displayAlert(title: "", message:  NSLocalizedString("EnterValidName", comment: ""), actionButtonTitle: NSLocalizedString("OK", comment: ""), controller: self)
            return
        }
        if carType.text == nil  ||  carType.text?.replace(" ", with: "") == "" {
            displayAlert(title: "", message: NSLocalizedString("SelectCarType", comment: "") , actionButtonTitle: NSLocalizedString("OK", comment: ""), controller: self)
            return
        }
        updateThroughFirebase()
    }
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUserExperience();
        initializeItemPicker()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func showPicker(_ sender: Any) {
        self.view.addSubview(itemToolBar)
        self.view.addSubview(itemPicker)
    }
    
    private func setupUserExperience() {
        let user = AppCacheData.sharedInstance.user
        self.fullName.text = user.fullName
        self.mobileNum.text = user.phoneNumber
        self.homeAddress.text = user.homeAddress
        self.workAddress.text = user.workAddress
        self.carType.text = user.carType
    }
    
    func initializeItemPicker(){
        
        itemPicker.frame = CGRect(x: 0, y: self.view.frame.size.height-220, width: self.view.frame.size.width, height: 220)
        itemPicker.backgroundColor = UIColor.white
        itemPicker.delegate = self
        itemPicker.dataSource = self
        
        itemToolBar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.size.height-264, width:self.view.frame.size.width, height: 44))
        let cancelBtn = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: "") , style: UIBarButtonItem.Style.done, target: self, action: #selector(itemPickerCancelAction))
        
        let doneBtn = UIBarButtonItem(title: NSLocalizedString("Select", comment: "") , style: UIBarButtonItem.Style.done, target: self, action: #selector(itemPickerDoneAction))
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        itemToolBar.setItems([cancelBtn, space, doneBtn], animated: false)
        
    }
    
    
    @objc func itemPickerDoneAction(){
        self.itemPicker.removeFromSuperview()
        self.itemToolBar.removeFromSuperview()
        carType.text = carTypes[selectedPickerRow]
    }
    
    @objc func itemPickerCancelAction(){
        self.itemPicker.removeFromSuperview()
        self.itemToolBar.removeFromSuperview()
    }
    func updateThroughFirebase() {
        if !Utilities.checkInternetAndShowAlert(){
            return
        }
        CustomLoader.sharedInstance.showActivityIndicator(uiView: self.view, title: "")
        let user = self.getUserModel()
        let database = YPDatabaseProvider<CustomDataModelUser>()
        database.request(.updateUser(user:user)) { (result) in
            switch result{
            case .success( _):
                CustomLoader.sharedInstance.hideActivityIndicator()
                AppCacheData.sharedInstance.user = user
                let jsonString = User.mapModelData(model: user)
                UserDefaults.standard.set(jsonString, forKey: Constant.UserDefaultKeys.userModel)
                UserDefaults.standard.set(false, forKey: isMonitoring)
                AlertCentral.displayOkAlert(title: "", message:NSLocalizedString("SuccessfullyUpdatedProfile", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in //localize
                }, controller: self)
            case .failure(_):
                self.showNetworkError()
            }
        }
    }
    
    private func getUserModel()->User {
        let user = AppCacheData.sharedInstance.user
        user.fullName = self.fullName.text!
        user.homeAddress = self.homeAddress.text ?? ""
        user.workAddress = self.workAddress.text ?? ""
        user.phoneNumber = self.mobileNum.text ?? ""
        user.carType = self.carType.text!
        user.homeAddressLocation = homeAddressLocation
        user.workAddressLocation = workAddressLocation
        return user
    }
    
    private func showNetworkError() {
        CustomLoader.sharedInstance.hideActivityIndicator()
        AlertCentral.displayOkAlert(title: "", message: NSLocalizedString("UnablePerformOperation", comment: ""), style: .alert, actionButtonTitle: NSLocalizedString("OK", comment: ""), completionHandler: { (action) in
        }, controller: self)
    }
    
}
extension ProfileViewController:UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
        
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return carTypes.count
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return carTypes[row]
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedPickerRow = row
        
    }
    
}
extension ProfileViewController:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == homeAddress || textField == workAddress {
            let storyboard = UIStoryboard(name: Utilities.StoryBoardName.Login.rawValue, bundle: nil)
            let mainViewController = storyboard.instantiateViewController(withIdentifier:"SelectLoctionViewController") as! SelectLoctionViewController
            mainViewController.delegate = self
            if textField == homeAddress {
                mainViewController.locationType = .home
            } else  {
                mainViewController.locationType = .work
            }
            self.present(mainViewController, animated: true, completion: {
            })
        }
        
    }
}
extension ProfileViewController:SelectLoctionViewControllerDelegate {
    func selectLoctionViewControllerDidSucesfullySelectedLocation(_ selectLoctionViewController:SelectLoctionViewController, with location:LocationModel,locationType:LocationType) {
        selectLoctionViewController.dismiss(animated: true, completion: nil)
        switch locationType {
        case .work:
            self.workAddressLocation = location
            self.workAddress.text = location.address
            self.workAddress.resignFirstResponder()
        case .home:
            self.homeAddressLocation = location
            self.homeAddress.text = location.address
            self.homeAddress.resignFirstResponder()
        }
    }
    func selectLoctionViewControllerDismisButtonPressed(_ selectLoctionViewController:SelectLoctionViewController) {
        selectLoctionViewController.dismiss(animated: true, completion: nil)
        
    }
}
